<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Rest/Error/ErrorAction/'
    . 'IsExpectedResponseCodeTestCase.php';

/**
 * Testing the behavior when controller of rest module not exists
 * 
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_Test_Functional_Controller_Error_ErrorAction_ControllerNotExistTest
extends Infrastructure_Test_Functional_Controller_Rest_Error_ErrorAction_IsExpectedResponseCodeTestCase
{
    protected function _getModuleName()
    {
        return 'rest';
    }
    
    protected function _getControllerName()
    {
        return 'controllerNotFound';
    }
    
    protected function _getActionName()
    {
        return 'actionNotFound';
    }
    
    protected function _getExpectedCodeResponse()
    {
        return 404;
    }
}