<?php
/**
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Information
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Testing if action of get returns the information about exception
 * when input parameter of url from post data not exists.
 * 
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Information
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_Test_Functional_InformationController_GetAction_IncorrectUrlTest
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * Verifies the behavior if action of get returns the information about
     * request from input parameter of url from post.
     * 
     * @return void
     */
    public function test_dispatchRequestWithIncorrectUrl_true()
    {
        $moduleName = $this->_getModuleName();
        $controllerName = $this->_getControllerName();
        $actionName = $this->_getActionName();
        
        
        $this->dispatch(
            "{$moduleName}/{$controllerName}/{$actionName}/"
        );
            $responseCode = $this->getResponse()->getHttpResponseCode();
                $isCorrectResponseCode = ($responseCode == 500);
            $responseBody = json_decode($this->getResponse()->getBody());
        
        
        $this->assertTrue($isCorrectResponseCode);
        $this->assertEquals($responseBody->code, 500);
        $this->assertEquals($responseBody->message, "Application error");
        $this->assertEquals(
            $responseBody->exception->message, "Invalid data to send!"
        );
    }
    
    protected function _getModuleName()
    {
        return 'rest';
    }
    
    protected function _getControllerName()
    {
        return 'information';
    }
    
    protected function _getActionName()
    {
        return 'get';
    }
}