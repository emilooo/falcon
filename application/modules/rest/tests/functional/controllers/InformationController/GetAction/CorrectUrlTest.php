<?php
/**
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Information
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/'
    . 'TestCase.php';

/**
 * Testing if action of get returns the information about request
 * when data of post stores information about correct url addresss.
 * 
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Information
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_Test_Functional_InformationController_GetAction_CorrectUrlTest
extends Infrastructure_Test_Functional_Controller_TestCase
{
    /**
     * Verifies the behavior if action of get returns the information about
     * request from input parameter of url from post.
     * 
     * @return void
     */
    public function test_dispatchRequestWithCorrectUrl_true()
    {
        $moduleName = $this->_getModuleName();
        $controllerName = $this->_getControllerName();
        $actionName = $this->_getActionName();
        $this->getRequest()->setMethod('POST')->setRawBody(
            json_encode($this->_getPostData())
        );
        
        
        $this->dispatch(
            "{$moduleName}/{$controllerName}/{$actionName}/"
        );
            $responseCode = $this->getResponse()->getHttpResponseCode();
                $isCorrectResponseCode = ($responseCode == 200);
            $responseBody = json_decode($this->getResponse()->getBody());
        
        
        $this->assertTrue($isCorrectResponseCode);
        $this->assertEquals($responseBody->module, "core");
        $this->assertEquals($responseBody->controller, "home");
        $this->assertEquals($responseBody->action, "index");
    }
    
    protected function _getModuleName()
    {
        return 'rest';
    }
    
    protected function _getControllerName()
    {
        return 'information';
    }
    
    protected function _getActionName()
    {
        return 'get';
    }
    
    /**
     * Returns the data to send by post
     * 
     * @return array
     */
    private function _getPostData()
    {
        $postData = array(
            'url' => 'http://example.com/'
        );
        
        return $postData;
    }
}