<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Rest/Email/PostAction/'
    . 'IncorrectDataToSendTestCase.php';

/**
 * Testing the behavior when action of rest module has error
 * 
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_Test_Functional_EmailController_PostAction_IncorrectDataToSendTest
extends Infrastructure_Test_Functional_Controller_Rest_Email_PostAction_IncorrectDataToSendTestCase
{
    protected function _getModuleName()
    {
        return 'rest';
    }
    
    protected function _getControllerName()
    {
        return 'email';
    }
    
    protected function _getActionName()
    {
        return 'post';
    }
    
    protected function _getIncorrectDataToSend()
    {
        return array(
            'sender' => 'invalidEmail',
            'subject' => 'in',
            'content' => 'i'
        );
    }
}