<?php
/**
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Rest/Email/PostAction/'
    . 'CorrectDataToSendTestCase.php';

/**
 * Testing the behavior when action of rest module has error
 * 
 * @category Test
 * @package Test_Rest_Functional_Controller
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_Test_Functional_EmailController_PostAction_CorrectDataToSendTest
extends Infrastructure_Test_Functional_Controller_Rest_Email_PostAction_CorrectDataToSendTestCase
{
    protected function _getModuleName()
    {
        return 'rest';
    }
    
    protected function _getControllerName()
    {
        return 'email';
    }
    
    protected function _getActionName()
    {
        return 'post';
    }
    
    protected function _getEmailTestDirectory()
    {
        $configuration = $this->_getConfiguration();
        
        return $configuration->email->testDirectory;
    }
    
    protected function _getCorrectDataToSend()
    {
        return array(
            'sender' => 'user@example.com',
            'subject' => 'Test message',
            'content' => 'Test content of message'
        );
    }
}
