<?php
/**
 * This source file is the part of emilooo.pl project
 *
 * @category Rest
 * @package Rest_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for to verifies the information about current
 * request for layer of front-end
 * 
 * @category Rest
 * @package Rest_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_InformationController
extends Infrastructure_Controller_Rest_Abstract
{
    public function headAction()
    {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function indexAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    /**
     * Returns the information about request for front-end layer in the format
     * of json. Method has a value of url from data of post where is full http
     * address.
     * 
     * @return string|json
     */
    public function getAction() {
        $getJsonDataResult = $this->getRequest()->getRawBody();
        if (!empty($getJsonDataResult)) {
            $getJsonToArrayResult = json_decode($getJsonDataResult);
            $requestToTesting
                = new Zend_Controller_Request_Http($getJsonToArrayResult->url);
            $getFrontControllerResult = Zend_Controller_Front::getInstance();
            $getRouterResult = $getFrontControllerResult->getRouter();
            $getCurrentRoute = $getRouterResult->route($requestToTesting);
            $informationAboutRequest = array(
                "module" => $getCurrentRoute->getModuleName(),
                "controller" => $getCurrentRoute->getControllerName(),
                "action" => $getCurrentRoute->getActionName(),
                "params" => $getCurrentRoute->getParams()
            );
            
            echo json_encode($informationAboutRequest);
        } else {
            throw new Exception('Invalid data to send!');
        }
    }
    
    public function putAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function postAction()
    {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function deleteAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
}