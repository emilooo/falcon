<?php
/**
 * @category Rest
 * @package Rest_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the interface to handling the operations of error controller
 * 
 * @category Rest
 * @package Rest_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_ErrorController
extends Infrastructure_Controller_Rest_Error_Abstract
{
    public function init()
    {
        parent::init();
        
        $this->_registerExceptionHandler(
            'Infrastructure_Acl_AccessDenied', 'AccessDenied'
        );
        $this->_registerExceptionHandler(
            'Zend_Controller_Dispatcher_Exception', 'ControllerNotFound'
        );
        $this->_registerExceptionHandler(
            'Zend_Controller_Action_Exception', 'ActionNotFound'
        );
    }
    
    public function indexAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function putAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function postAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function deleteAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function getAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Uncompatible action'
        );
    }
    
    public function headAction() {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
}