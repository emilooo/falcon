<?php
/**
 * @category Rest
 * @package Rest_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the interface to support the process of sending email message
 * with level of rest interface
 * 
 * @category Rest
 * @package Rest_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_EmailController
extends Infrastructure_Controller_Rest_Email_Abstract
{
    public function headAction()
    {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function indexAction()
    {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function getAction()
    {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function putAction()
    {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
    
    public function deleteAction()
    {
        throw new Infrastructure_Exception_ActionHasError(
            'Action is incompatible!'
        );
    }
}