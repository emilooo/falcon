<?php
/**
 * @category Rest
 * @package Rest_Action
 * @subpackage EmailController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the interface to sending email message
 * 
 * This action mediates in the process of sending email message
 * 
 * @category Rest
 * @package Rest_Action
 * @subpackage EmailController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_Action_EmailController_Post
extends Infrastructure_Action_Rest_Email_Abstract
{
    protected function _makeEmailService()
    {
        return new Contact_Service_Email();
    }
}