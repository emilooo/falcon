<?php
/**
 * This source file is part of content management system
 *
 * @category Rest
 * @package Rest_Action
 * @subpackage ErrorController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the behavior when action not exists
 * 
 * @category Rest
 * @package Rest_Action
 * @subpackage ErrorController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Rest_Action_ErrorController_ActionNotFound
extends Infrastructure_Action_Rest_Error_Abstract
{
    public function init()
    {
    }
    
    protected function _getResponseCode()
    {
        return 404;
    }
    
    protected function _getResponseMessage()
    {
        return 'Operation not found';
    }
}
