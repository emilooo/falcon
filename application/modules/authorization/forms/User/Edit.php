<?php
/**
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Generates form for edit user
 * 
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Form_User_Edit
extends Authorization_Form_User_Base
{
    public function init()
    {
        parent::init();
        
        $this->setDecorators(array(
            array('ViewScript', array(
                'viewScript' => "_adminFormTemplate.tpl",
                'viewModule' => 'core')
            )
        ));
        $this->removeElement('password');
        $this->removeElement('passwordrepeat');
        $this->removeElement('register');
        $this->getElement('email')->removeValidator('UniqueEmail');
    }
}