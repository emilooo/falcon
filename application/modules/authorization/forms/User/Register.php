<?php
/**
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Generates form for register users
 * 
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Form_User_Register
extends Authorization_Form_User_Base
{
    public function init()
    {
        parent::init();
        
        $this->removeElement('id');
        $this->removeElement('role');
        $this->removeElement('status');
    }
}