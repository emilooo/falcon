<?php
/**
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Generates form for login user
 * 
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Form_User_Login
extends Authorization_Form_User_Base
{
    public function init()
    {
        parent::init();
        
        $this->removeElement('id');
        $this->removeElement('firstname');
        $this->removeElement('lastname');
        $this->removeElement('login');
        $this->removeElement('role');
        $this->removeElement('status');
        $this->removeElement('passwordrepeat');
        $this->getElement('email')->removeValidator('UniqueEmail');
        $this->getElement('register')->setLabel('Login')->setName('login');
    }
}