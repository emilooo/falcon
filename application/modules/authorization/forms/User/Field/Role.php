<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the role form element
 * 
 * @category Authorization
 * @package Authorization_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Form_User_Field_Role
implements Infrastructure_Form_Element_Interface
{
    public function getElement()
    {
        $createElementResult = $this->_createElement();
        $createElementResult->addValidator($this->_createNotEmptyValidator());
        $createElementResult->setRequired(true);
        $createElementResult
            ->setAttrib('helpMessage', $this->_getHelpMessage());
        
        return $createElementResult;
    }
    
    private function _createElement()
    {
        $roleElement = new Zend_Form_Element_Select(
            'role',
            array(
                'label' => 'UserFormRoleLabel: ',
                'multiOptions' => array(
                    0 => 'Select role: ',
                    'Guest' => 'Guest',
                    'User' => 'User',
                    'Admin' => 'Admin'
                )
            )
        );
        
        return $roleElement;
    }
    
    /**
     * Returns the instance of notEmptyValidator
     * 
     * @return \Zend_Validate_NotEmpty
     */
    private function _createNotEmptyValidator()
    {
        $emptyValidator = new Zend_Validate_NotEmpty();
        $emptyValidator->setType (
            $emptyValidator->getType() | Zend_Validate_NotEmpty::INTEGER
            | Zend_Validate_NotEmpty::ZERO);
        
        return $emptyValidator;
    }
    
    /**
     * Returns the help message
     * 
     * @return string
     */
    private function _getHelpMessage()
    {
        return 'Select the group of user';
    }
}
