<?php
/**
 * @category Authorization
 * @package Authorization_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the status form element
 * 
 * @category Authorization
 * @package Authorization_Form
 * @subpackage Field
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Form_User_Field_Status
implements Infrastructure_Form_Element_Interface
{
    public function getElement()
    {
        $createElementResult = $this->_createElement();
        $createElementResult->addValidator($this->_createNotEmptyValidator());
        $createElementResult->setRequired(true);
        $createElementResult
            ->setAttrib('helpMessage', $this->_getHelpMessage());
        
        return $createElementResult;
    }
    
    private function _createElement()
    {
        $roleElement = new Zend_Form_Element_Select(
            'status',
            array(
                'label' => 'UserFormStatusLabel: ',
                'multiOptions' => array(
                    0 => 'Select status: ',
                    'Unconfirmed' => 'Unconfirmed',
                    'Inactive' => 'Inactive',
                    'Active' => 'Active'
                )
            )
        );
        
        return $roleElement;
    }
    
    /**
     * Returns the instance of notEmptyValidator
     * 
     * @return \Zend_Validate_NotEmpty
     */
    private function _createNotEmptyValidator()
    {
        $emptyValidator = new Zend_Validate_NotEmpty();
        $emptyValidator->setType (
            $emptyValidator->getType() | Zend_Validate_NotEmpty::INTEGER
            | Zend_Validate_NotEmpty::ZERO);
        
        return $emptyValidator;
    }
    
    /**
     * Returns the help message
     * 
     * @return string
     */
    private function _getHelpMessage()
    {
        return 'Select the status of user';
    }
}

