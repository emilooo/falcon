<?php
/**
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for all user forms
 * 
 * @category Authorization
 * @package Authorization_Form
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Form_User_Base
extends Infrastructure_Form_Abstract
{
    public function init()
    {
        $this->addElementPrefixPath(
            'Authorization_Model_Validator',
            APPLICATION_PATH . '/modules/authorization/models/validators/',
            'validate'
        );
        
        $this->_createId();
        $this->_createLogin();
        $this->_createFirstname();
        $this->_createLastname();
        $this->_createEmail();
        $this->_createRole();
        $this->_createStatus();
        $this->_createPassword();
        $this->_createPasswordRepeat();
        $this->_createRegister();
        $this->setDefaults(
            array(
                'role' => 0,
                'status' => 0
            )
        );
        $this->setName('userForm');
    }
    
    private function _createId()
    {
        $this->addElement(
            'hidden', 'id', array(
                'filters' => array(
                    array('Digits', 'StringTrim'),
                ),
                'validators' => array(
                    array('NotEmpty'),
                    array('Digits', true),
                    array('StringLength', true, array('min'=>1, 'max'=>9)),
                ),
                'required' => true,
            )
        );
    }
    
    private function _createLogin()
    {
        $this->addElement(
            'text', 'login', array(
                'required' => true,
                'filters' => array('Alnum','StringTrim'),
                'validators' => array(
                    array('NotEmpty', true),
                    array('Alnum', true),
                    array('StringLength', true, array('min'=>3, 'max'=> 20)),
                    array('UniqueLogin', false, array(new Authorization_Model_User()))
                ),
                'label'=>'UserFormLoginLabel:',
            )
        );
        $this->getElement('login')
            ->setAttrib('helpMessage', 'Insert the login of user');
    }
    
    private function _createFirstname()
    {
        $this->addElement(
            'text', 'firstname', array(
                'required' => true,
                'filters' => array('Alpha','StringTrim'),
                'validators' => array(
                    array('NotEmpty', true),
                    array('Alpha', true),
                    array('StringLength', true, array('min'=>3, 'max'=> 20)),
                ),
                'label'=>'UserFormFirstnameLabel:'
            )
        );
        $this->getElement('firstname')
            ->setAttrib('helpMessage', 'Insert the firstname of user');
    }
    
    private function _createLastname()
    {
        $this->addElement(
            'text', 'lastname', array(
                'required' => true,
                'filters' => array('Alpha','StringTrim'),
                'validators' => array(
                    array('NotEmpty', true),
                    array('Alpha', true),
                    array('StringLength', true, array('min'=>3, 'max'=> 20)),
                ),
                'label'=>'UserFormLastnameLabel:'
            )
        );
        $this->getElement('lastname')
            ->setAttrib('helpMessage', 'Insert the lastname of user');
    }
    
    private function _createEmail()
    {
        $this->addElement(
            'text', 'email', array(
                'required' => true,
                'filters' => array('StringTrim'),
                'validators' => array(
                    array('StringLength', true, array('min'=>3, 'max'=> 50)),
                    array('EmailAddress', true),
                    array('UniqueEmail', false, array(new Authorization_Model_User())),
                ),
                'label'=>'UserFormEmailLabel:'
            )
        );
        $this->getElement('email')
            ->setAttrib('helpMessage', 'Insert the e-mail of user');
    }
    
    /**
     * @return \Authorization_Form_Field_Role
     */
    private function _createRole()
    {
        $roleElement = new Authorization_Form_User_Field_Role();
        
        $this->addElement($roleElement->getElement());
    }
    
    private function _createStatus()
    {
        $statusElement = new Authorization_Form_User_Field_Status();
        
        $this->addElement($statusElement->getElement());
    }
    
    private function _createPassword()
    {
        $this->addElement(
            'password', 'password', array(
                'required' => true,
                'filters' => array('Alnum','StringTrim'),
                'validators' => array(
                    array('NotEmpty', true),
                    array('Alnum', true),
                    array('StringLength', true, array('min'=>3, 'max'=> 20)),
                ),
                'label'=>'UserFormPasswordLabel:'
            )
        );
        $this->getElement('password')
            ->setAttrib('helpMessage', 'Insert the password of user');
    }
    
    private function _createPasswordRepeat()
    {
        $this->addElement(
            'password', 'passwordrepeat', array(
                'required' => true,
                'filters' => array('Alnum','StringTrim'),
                'validators' => array(
                    array('NotEmpty', true),
                    array('Alnum', true),
                    array('StringLength', true, array('min'=>3, 'max'=> 20)),
                    array('PasswordVerification'),
                ),
                'label'=>'UserFormPasswordrepeatLabel:'
            )
        );
        $this->getElement('passwordrepeat')
            ->setAttrib('helpMessage', 'Repeat the password of user');
    }
    
    private function _createRegister()
    {
        $this->addElement(
            'submit',
            'register', array('label'=>'UserFormRegisterLabel')
        );
    }
}