<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Model_Resource
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides interface for row of pages database table
 * 
 * @category Authorization
 * @package Authorization_Model_Resource
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Model_Resource_User_Row
extends Zend_Db_Table_Row_Abstract
{
}