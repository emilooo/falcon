<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation of crud resource model
 * 
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Model_Resource_User
extends Infrastructure_Model_Resource_Db_Table_Crud_Abstract
{
    public function init()
    {
        $this->_name = 'users';
        $this->_primary = 'user_id';
        $this->_rowClass = 'Authorization_Model_Resource_User_Row';
        
        parent::init();
    }
    
    /**
     * Returns the salt string
     * 
     * @return string
     */
    private function _generateSalt()
    {
        $salt = md5(time());
        return $salt;
    }
}