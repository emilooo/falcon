<?php
/**
 * @package Structure
 * @package Structure_Model
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for implements of access control list for models
 * 
 * @package Structure
 * @package Structure_Model
 * @subpackage Acl
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Model_Acl_Authorization
extends Zend_Acl
implements Infrastructure_Acl_Interface
{
    public function __construct()
    {
        $this->addRole(new Structure_Model_Acl_Role_Guest);
        $this->addRole(new Structure_Model_Acl_Role_User, 'Guest');
        $this->addRole(new Structure_Model_Acl_Role_Admin, 'User');
        
        $this->deny();
        
        $this->add(new Structure_Model_Acl_Resource_Admin)->allow('Admin');
    }
}