<?php
/**
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Validator
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for validation password from form
 * 
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Validator
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Model_Validator_PasswordVerification
extends Zend_Validate_Abstract
{
    const NOT_MATCH = 'notMatch';
    
    protected $_messageTemplates = array(
        self::NOT_MATCH => 'Passwords not match'
    );
    
    /**
     * Checking does value is equal to context
     * 
     * @param string $value
     * @param string|array $context
     * @return boolean
     */
    public function isValid($value, $context = null)
    {
        $value = (string) $value;
        $this->_setValue($value);
        
        if (is_array($context)) {
            if (isset($context['password'])
                && ($value == $context['password'])) {
                return true;
            }
        } elseif (is_string($context) && ($value == $context)) {
            return true;
        }
        
        $this->_error(self::NOT_MATCH);
        
        return false;
    }
}