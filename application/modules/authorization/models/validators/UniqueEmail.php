<?php
/**
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Validator
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for checking does email not exist in database table
 * 
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Validator
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Model_Validator_UniqueEmail
extends Zend_Validate_Abstract
{
    const EMAIL_EXISTS = 'emailExists';
    
    protected $_messageTemplates = array(
        self::EMAIL_EXISTS=>'Email %value% already exists in our system',
    );
    
    public function __construct(Authorization_Model_User $model)
    {
        $this->_model = $model;
    }
    
    /**
     * Checking does email address not exist in database table
     * 
     * @param string $value Email address to check
     * @param array $context
     * @return boolean Returns true when record don't exist
     */
    public function isValid($value, $context = null)
    {
        $this->_setValue($value);
        try {
            $readOneItemByEmailResult = $this->_model->readOneItemByEmail(
                $value
            );
            if (empty($readOneItemByEmailResult)) {
                return true;
            }
            $this->_error(self::EMAIL_EXISTS);
            return false;
        } catch (Exception $ex) {
            return true;
        }
    }
}

