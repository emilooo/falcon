<?php
/**
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Validator
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for checking does email not exist in database table
 * 
 * @category Authorization
 * @package Authorization_Model
 * @subpackage Validator
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Model_Validator_UniqueLogin
extends Zend_Validate_Abstract
{
    const LOGIN_EXISTS = 'loginExists';
    
    protected $_messageTemplates = array(
        self::LOGIN_EXISTS=>'Login %value% already exists in our system',
    );
    
    public function __construct(Authorization_Model_User $model)
    {
        $this->_model = $model;
    }
    
    /**
     * Checking does loginnot exist in database table
     * 
     * @param string $value Login to check
     * @param array $context
     * @return boolean Returns true when record don't exist
     */
    public function isValid($value, $context = null)
    {
        $this->_setValue($value);
        try {
            $readOneItemByLoginResult = $this->_model->readOneItemByLogin(
                $value
            );
            if (empty($readOneItemByLoginResult)) {
                return true;
            }
            
            $this->_error(self::LOGIN_EXISTS);
            return false;
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            return true;
        }
    }
}