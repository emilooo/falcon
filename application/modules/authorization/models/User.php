<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the interface of basic implementation of crud model
 * 
 * @category Authorization
 * @package Authorization_Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Model_User
extends Infrastructure_Model_Auth_Abstract
{
    /**
     * Stores the static value for row column of user_salt
     * 
     * @var string
     */
    public $salt = null;
    
    public function init()
    {
        $this->updateForm = 'Edit';
    }
    
    /**
     * This method sets a this model as resource of access control
     * list and sets access roles for all reserved methods in this model
     * 
     * @param Infrastructure_Acl_Interface $acl
     * @return Authorization_Model_User
     */
    public function setAcl(Infrastructure_Acl_Interface $acl)
    {
        $getResourceIdResult = $this->getResourceId();
        if (!$acl->has($getResourceIdResult)) {
            $acl->add($this)
                ->allow('Guest', $this, array())
                ->deny(
                    'Guest', $this, array(
                        'createItem', 'updateItems', 'deleteItem',
                        'readOneItem', 'readManyItems'
                    )
                )->allow(
                    'User',
                    $this,
                    array(
                        'readOneItem',
                        'readManyItems'
                    )
                )->allow('Admin', $this);
        }
        $this->_acl = $acl;
        return $this;
    }
    
    /**
     * Returns current instance of access control list
     * 
     * @return Authorization_Model_Acl_Authorization
     */
    public function getAcl()
    {
        if (empty($this->_acl)) {
            $modelAcl = new Authorization_Model_Acl_Authorization();
            $this->setAcl($modelAcl);
        }
        
        return $this->_acl;
    }
    
    public function getResourceId()
    {
        return 'User';
    }
    
    public function updateItem($id, $dataToUpdate)
    {
        if (!$this->checkAcl('updateItems')) {
            $this->_throwAccessDeniedException();
        }
        
        $isCorrectId = (is_numeric($id) || is_int($id));
            assert($isCorrectId);
        $getUpdateFormResult = $this->getForm($this->updateForm);
        if ($getUpdateFormResult->isValid($dataToUpdate)) {
            $transformColumnsResult
                = $this->_transformColumns($getUpdateFormResult->getValues());
            return $this->getResource($this->getResourceId())->updateItems(
                'user_id', $id, $transformColumnsResult
            );
        }
        
        return false;
    }
    
    public function deleteItem($id)
    {
        if (!$this->checkAcl('deleteItem')) {
            $this->_throwAccessDeniedException();
        }
        
        $isCorrectId = (is_int($id) || is_numeric($id));
            assert($isCorrectId);
        
        return $this->getResource($this->getResourceId())
            ->deleteItems('user_id', (int) $id);
    }
    
    public function readOneItem($id)
    {
        if (!$this->checkAcl('readOneItem')) {
            $this->_throwAccessDeniedException();
        }
        $isCorrectId = (is_numeric($id) || is_int($id));
            assert($isCorrectId);
        return $this->getResource($this->getResourceId())
            ->readOneItem('user_id', (int) $id);
    }
    
    /**
     * Returns the item through email
     * 
     * Important - access to this method must be public because with
     * this method using authentication service.
     * 
     * @return Zend_Db_Table_Row
     * @throws Infrastructure_Model_Resource_Db_Table_Row_NotExist
     */
    public function readOneItemByEmail($email)
    {
        $isCorrectEmail = is_string($email);
            assert($isCorrectEmail);
        
        return $this->getResource($this->getResourceId())
            ->readOneItem('user_email', $email);
    }
    
    /**
     * Returns the item through login
     * 
     * Important - access to this method must be public because with
     * this method using uniqueLogin validator
     * 
     * @return Zend_Db_Table_Row
     * @throws Infrastructure_Model_Resource_Db_Table_Row_NotExist
     */
    public function readOneItemByLogin($login)
    {
        $isCorrectLogin = is_string($login);
            assert($isCorrectLogin);
        
        return $this->getResource($this->getResourceId())
            ->readOneItem('user_login', $login);
    }
    
    protected function _transformColumns($formFields)
    {
        $tableColumns = array();
        foreach ($formFields as $columnName => $columnValue) {
            $columnName = 'user_' . $columnName;
            $tableColumns[$columnName] = $columnValue;
        }
        
        $isPasswordField = isset($formFields['password']);
        if ($isPasswordField) {
            $isSaltToTesting = (!empty($this->salt));
            $newSalt = ($isSaltToTesting) ? $this->salt : md5(time());
            $tableColumns["user_salt"] = $newSalt;
            $tableColumns["user_password"]
                = md5($formFields["password"] . $newSalt);
        }
        
        $isRoleField = isset($formFields['role']);
        $tableColumns['user_role']
            = ($isRoleField)
                ? $formFields['role'] : 'User';
        
        $isStatusField = isset($formFields["status"]);
        $tableColumns["user_status"]
            = ($isStatusField)
                ? $formFields["status"] : "Unconfirmed";
        
        return parent::_transformColumns($tableColumns);
    }
}