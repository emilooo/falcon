<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation to handling controller action of read
 * 
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Action_UserController_Read
extends Infrastructure_Action_Http_Crud_Read
{
    public function hasAccessToAction()
    {
        parent::hasAccessToAction();
        
        return $this->getController()->getHelper('Acl')->checkAcl('Admin');
    }
    
    public function doWhenAccessToActionIsDenied()
    {
        parent::doWhenAccessToActionIsDenied();
        
        $this->getController()->getHelper('FlashMessenger')
            ->addMessage('You haven\'t privileges to this action');
        $this->getController()->getHelper('Redirector')
            ->gotoRoute(array(), 'listUsers');
    }
    
    public function doWhenActionSuccessfullyMaked()
    {
        parent::doWhenActionSuccessfullyMaked();
        
        return $this->getController()->render();
    }
    
    public function doWhenActionUnsuccessfullyMaked()
    {
        parent::doWhenActionUnsuccessfullyMaked();
        
        throw new Exception('Record not exist!');
    }
    
    public function appendToPageTitle(Zend_View_Helper_HeadTitle $headTitle)
    {
    }
    
    public function appendToPageMetaTags(
        Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageStyles(
        Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageScripts(
        Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
    }
    
    public function appendToTemplate(\Zend_View_Helper_Layout $layoutHelper)
    {
    }
    
    protected function _getViewRendererName()
    {
    }
}