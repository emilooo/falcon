<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation to handling controller action of register
 * 
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Action_UserController_Register
extends Infrastructure_Action_Http_Auth_Register
{
    public function doWhenActionSuccessfullyMaked()
    {
        parent::doWhenActionSuccessfullyMaked();
        
        return $this->getController()->getHelper('Redirector')
            ->gotoRoute(array(), 'loginUser');
    }
    
    public function doWhenActionUnsuccessfullyMaked()
    {
        parent::doWhenActionUnsuccessfullyMaked();
        
        return $this->getController()->render();
    }
    
    public function appendToPageTitle(Zend_View_Helper_HeadTitle $headTitle)
    {
        $translatedHeadTitle
            = $headTitle->getTranslator()->translate('Register');
        $headTitle->headTitle($translatedHeadTitle);
    }
    
    public function appendToPageMetaTags(
        Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageStyles(
        Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageScripts(
        Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
        $headScriptHelper->setAllowArbitraryAttributes(true);
        $headScriptHelper->appendFile(
            '/js/library/requirejs/require.js',
            'text/javascript',
            array('data-main'=>'/js/application/setup')
        );
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
        $headLinkHelper->appendStylesheet(
            "/css/library/jquery-ui/themes/base/jquery-ui.css"
        );
        $headLinkHelper->appendStylesheet(
            '/css/templates/login/screen.css'
        );
    }
    
    public function appendToTemplate(\Zend_View_Helper_Layout $layoutHelper)
    {
        $layoutHelper->layout()->setLayout('login');
    }
    
    protected function _getForm()
    {
        return $this->getController()->getModel()->getForm('Register');
    }
    
    protected function _getAuthenticationService()
    {
        return new Authorization_Service_Authentication();
    }
    
    protected function _getViewRendererName()
    {
        return 'register-user';
    }
}
