<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation to handling controller action of login
 * 
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Action_UserController_Login
extends Infrastructure_Action_Http_Auth_Login
{
    public function doWhenActionSuccessfullyMaked()
    {
        parent::doWhenActionSuccessfullyMaked();
        
        return $this->getController()->getHelper('Redirector')
            ->gotoRoute(array(), 'listUsers');
    }
    
    public function doWhenActionUnsuccessfullyMaked()
    {
        parent::doWhenActionUnsuccessfullyMaked();
        
        return $this->getController()->render();
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageMetaTags(
        \Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageScripts(
        \Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
        $headScriptHelper->setAllowArbitraryAttributes(true);
        $headScriptHelper->appendFile(
            '/js/library/requirejs/require.js',
            'text/javascript',
            array('data-main'=>'/js/application/setup')
        );
    }
    
    public function appendToPageStyles(
        \Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageTitle(
        \Zend_View_Helper_HeadTitle $headTitleHelper
    )
    {
        $translatedHeadTitle
            = $headTitleHelper->getTranslator()->translate('Login');
        $headTitleHelper->headTitle($translatedHeadTitle);
    }
    
    public function appendToTemplate(
        \Zend_View_Helper_Layout $layoutHelper
    )
    {
        $layoutHelper->layout()->setLayout('login');
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
        $headLinkHelper->appendStylesheet(
            "/css/library/jquery-ui/themes/base/jquery-ui.css"
        );
        $headLinkHelper->appendStylesheet(
            '/css/templates/login/screen.css'
        );
    }
    
    protected function _getForm()
    {
        return new Authorization_Form_User_Login();
    }
    
    protected function _getAuthenticationService() {
        return new Authorization_Service_Authentication();
    }
    
    protected function _getViewRendererName()
    {
    }
}