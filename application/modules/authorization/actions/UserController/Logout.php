<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation to handling controller action of logout
 * 
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Action_UserController_Logout
extends Infrastructure_Action_Http_Auth_Logout
{
    public function doWhenActionSuccessfullyMaked()
    {
        parent::doWhenActionSuccessfullyMaked();
        
        return $this->getController()->getHelper('Redirector')
            ->gotoRoute(array(), 'loginUser');
    }
    
    public function doWhenActionUnsuccessfullyMaked() {
        parent::doWhenActionUnsuccessfullyMaked();
        
        return $this->getController()->getHelper('Redirector')
            ->gotoRoute(array(), 'loginUser');
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageMetaTags(
        \Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageScripts(
        \Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
    }
    
    public function appendToPageStyles(
        \Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageTitle(
        \Zend_View_Helper_HeadTitle $headTitleHelper
    )
    {
    }
    
    public function appendToTemplate(
        \Zend_View_Helper_Layout $layoutHelper
    )
    {
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
    }
    
    protected function _getAuthenticationService()
    {
        return new Authorization_Service_Authentication();
    }
    
    protected function _getViewRendererName()
    {
    }
}