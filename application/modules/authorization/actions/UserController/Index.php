<?php
/**
 * This source file is part of content management system
 *
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation to handling controller action of index
 * 
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Action_UserController_Index
extends Infrastructure_Action_Http_Crud_Index
{
    public function hasAccessToAction()
    {
        parent::hasAccessToAction();
        
        return $this->getController()->getModel()->checkAcl('readManyItems');
    }
    
    public function doWhenAccessToActionIsDenied()
    {
        parent::doWhenAccessToActionIsDenied();
        
        $this->getController()->getHelper('FlashMessenger')
            ->addMessage('You haven\'t privileges to this action');
        $this->getController()->getHelper('Redirector')
            ->gotoRoute(array(), 'loginUser');
    }
    
    public function doWhenActionSuccessfullyMaked()
    {
        parent::doWhenActionSuccessfullyMaked();
        
        return $this->getController()->render();
    }
    
    public function appendToPageTitle(Zend_View_Helper_HeadTitle $headTitle)
    {
        $translator=$headTitle->getTranslator();
        $translatedTitle=$translator->translate('List of results');
        $headTitle->headTitle($translatedTitle);
    }
    
    public function appendToPageMetaTags(
        Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageStyles(
        Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageScripts(
        Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
        $headScriptHelper->setAllowArbitraryAttributes(true);
        $headScriptHelper->appendFile(
            '/js/library/requirejs/require.js',
            'text/javascript',
            array('data-main'=>'/js/application/setup')
        );
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
        $headLinkHelper->appendStylesheet(
            "/css/library/jquery-ui/themes/base/jquery-ui.css"
        );
        $headLinkHelper->appendStylesheet(
            '/css/templates/admin/screen.css'
        );
    }
    
    public function appendToTemplate(\Zend_View_Helper_Layout $layoutHelper)
    {
    }
    
    protected function _getViewRendererName()
    {
    }
}