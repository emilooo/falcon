<?php
/**
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic implementation to handling controller action of read
 * 
 * @category Authorization
 * @package Authorization_Action
 * @subpackage UserController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Action_UserController_Profile
extends Infrastructure_Action_Http_Auth_Profile
{
    public function hasAccessToAction()
    {
        parent::hasAccessToAction();
        
        if ($this->getController()->getHelper('Acl')->getIdentity() === "Guest") {
		return false;
	}

	return true;
    }
    
    public function doWhenAccessToActionIsDenied()
    {
        parent::doWhenAccessToActionIsDenied();
        
        $this->getController()->getHelper('FlashMessenger')
            ->addMessage('You haven\'t privileges to this action');
        $this->getController()->getHelper('Redirector')
            ->gotoRoute(array(), 'listUsers');
    }
    
    public function doWhenActionSuccessfullyMaked()
    {
        parent::doWhenActionSuccessfullyMaked();
        
        return $this->getController()->render();
    }
    
    public function doWhenActionUnsuccessfullyMaked()
    {
        parent::doWhenActionUnsuccessfullyMaked();
        
        throw new Exception('Profile not exist!');
    }
    
    public function appendToPageTitle(Zend_View_Helper_HeadTitle $headTitle)
    {
        $translator=$headTitle->getTranslator();
        $translatedTitle=$translator->translate('Profile');
        $headTitle->headTitle($translatedTitle);
    }
    
    public function appendToPageMetaTags(
        Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageStyles(
        Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageScripts(
        Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
        $headScriptHelper->setAllowArbitraryAttributes(true);
        $headScriptHelper->appendFile(
            '/js/library/requirejs/require.js',
            'text/javascript',
            array('data-main'=>'/js/application/setup')
        );
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
        $headLinkHelper->appendStylesheet(
            "/css/library/jquery-ui/themes/base/jquery-ui.css"
        );
        $headLinkHelper->appendStylesheet(
            '/css/templates/admin/screen.css'
        );
    }
    
    public function appendToTemplate(\Zend_View_Helper_Layout $layoutHelper)
    {
    }
    
    protected function _getViewRendererName()
    {
        return 'profile-user';
    }
}
