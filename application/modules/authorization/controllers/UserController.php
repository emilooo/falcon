<?php
/**
 * @category Authorization
 * @package Authorization_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface to manage users in system
 * 
 * @category Authorization
 * @package Authorization_Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_UserController
extends Infrastructure_Controller_Http_Auth_Abstract
{
    /**
     * Stores the instance of curd mdoel
     * 
     * @var Infrastructure_Model_Crud_Abstract
     */
    private $_crudModel;
    
    /**
     * Stores the instance of create form
     * 
     * @var Infrastructure_Form_Abstract
     */
    private $_createForm;
    
    /**
     * Stores the instance of update form
     * 
     * @var Infrastructure_Form_Abstract
     */
    private $_updateForm;
    
    /**
     * Stores the instance of login form
     * 
     * @var Infrastructure_Form_Abstract
     */
    private $_loginForm;
    
    /**
     * Stores the instance of register form
     * 
     * @var Infrastructure_Form_Abstract
     */
    private $_registerForm;
    
    /**
     * Stores the instance of remind form
     * 
     * @var Infrastructure_Form_Abstract
     */
    private $_remindForm;
    
    public function init()
    {
        parent::init();
        
        $this->_crudModel = new Authorization_Model_User();
    }
    public function setPageBreadcrumbs()
    {
    }
    
    public function setPageLinks(\Zend_View_Helper_HeadLink $headLinkHelper)
    {
    }
    
    public function setPageMetaTags(\Zend_View_Helper_HeadMeta $headMetaHelper)
    {
    }
    
    public function setPageScripts(\Zend_View_Helper_HeadScript $headScriptHelper)
    {
    }
    
    public function setPageStyles(\Zend_View_Helper_HeadStyle $headStyleHelper)
    {
    }
    
    public function setPageTemplate(\Zend_View_Helper_Layout $layoutHelper)
    {
        $layoutHelper->layout()->setLayout('admin');
    }
    
    public function setPageTitle(\Zend_View_Helper_HeadTitle $headTitleHelper)
    {
    }
    
    public function getModel()
    {
        return $this->_crudModel;
    }
}