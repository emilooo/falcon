<?php
/**
 * @category Authorization
 * @package Authorization_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for menage to tokens. The token is unique string
 * value for 
 * 
 * @category Authorization
 * @package Authorization_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Service_Token
{
    /**
     * Stores the token
     * 
     * @var string
     */
    private $_token;
    
    /**
     * Generates the token
     * 
     * @return boolean
     */
    public function generate()
    {
        $token = uniqid(rand(), TRUE);
        $alnumFilter = new Zend_Filter_Alnum();
        $filteredToken = $alnumFilter->filter($token);
        $this->set($filteredToken);
        
        return (empty($this->_token));
    }
    
    /**
     * Returns the token
     * 
     * @return string
     */
    public function get()
    {
        if (empty($this->_token)) {
            $this->generate();
        }
        
        return $this->_token;
    }
    
    /**
     * Sets the content of token
     * 
     * @param string $tokenValue
     * @return boolean
     */
    public function set($tokenValue)
    {
        $isCorrectToken = is_string($tokenValue);
            assert($isCorrectToken);
        
        $this->_token = $tokenValue;
        
        return ($this->_token == $tokenValue);
    }
}