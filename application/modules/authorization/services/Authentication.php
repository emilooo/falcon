<?php
/**
 * @package Authorization
 * @package Authorization_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * The authentication service provides authentication services
 * for the module of front
 * 
 * @package Authorization
 * @package Authorization_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Service_Authentication
{
    /**
     * @var Zend_Auth_Adapter_DbTable
     */
    protected $_zendAuthAdapter;
    
    /**
     * @var Authorization_Model_User
     */
    protected $_userModel;
    
    /**
     * @var Zend_Auth
     */
    protected $_zendAuth;
    
    /**
     * @param null|Authorization_Model_User $userModel
     */
    public function __construct(Authorization_Model_User $userModel = null)
    {
        $this->_userModel = ($userModel === null)
            ? new Authorization_Model_User() : $userModel;
    }
    
    /**
     * Authorizes the user
     * 
     * @param array $credentials Matched pair array containing
     * email and password
     * @return boolean
     */
    public function authenticate($credentials)
    {
        $zendAuthAdapter = $this->getAuthAdapter($credentials);
        $zendAuth = $this->getAuth();
        $result = $zendAuth->authenticate($zendAuthAdapter);
            if (!$result->isValid()) {
                return false;
            }
        
        $this->_saveIdentityToSession($credentials);
        return true;
    }
    
    /**
     * @return Zend_Auth
     */
    public function getAuth()
    {
        if ($this->_zendAuth === null) {
            $this->_zendAuth = Zend_Auth::getInstance();
        }
        
        return $this->_zendAuth;
    }
    
    /**
     * @return boolean|string
     */
    public function getIdentity()
    {
        $auth = $this->getAuth();
        if ($auth->hasIdentity()) {
            return $auth->getIdentity();
        }
        
        return false;
    }
    
    public function clear()
    {
        $this->getAuth()->clearIdentity();
    }
    
    /**
     * @param Zend_Auth_Adapter_Interface $adapter
     */
    public function setAuthAdapter(Zend_Auth_Adapter_Interface $adapter)
    {
        $this->_zendAuthAdapter = $adapter;
    }
    
    /**
     * @param array $credentials Matched pair array containing
     * email and password
     * @return Zend_Db_Table_Abstract
     */
    public function getAuthAdapter($credentials)
    {
        if ($this->_zendAuthAdapter === null) {
            $zendAuthAdapter = new Zend_Auth_Adapter_DbTable(
                Zend_Db_Table_Abstract::getDefaultAdapter(),
                "users",
                "user_email",
                "user_password",
                "MD5(CONCAT(?, user_salt)) AND user_status='active'"
            );
            
            $this->setAuthAdapter($zendAuthAdapter);
            $this->_zendAuthAdapter->setIdentity($credentials['email']);
            $this->_zendAuthAdapter->setCredential($credentials['password']);
        }
        
        return $this->_zendAuthAdapter;
    }
    
    /**
     * Saves the row to session through data of credentals
     * 
     * @param array $credentials
     * @return boolean
     */
    private function _saveIdentityToSession($credentials)
    {
        assert(is_array($credentials) && !empty($credentials));
        
        $zendAuth = $this->getAuth();
        $getUserByEmailResult = $this->_userModel
            ->readOneItemByEmail($credentials['email']);
        $zendAuth->getStorage()->write(
            $getUserByEmailResult
        );
        
        return (!$zendAuth->getStorage()->isEmpty());
    }
}