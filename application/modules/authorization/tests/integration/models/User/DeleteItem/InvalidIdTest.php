<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Crud/DeleteItem/'
    . 'InvalidIdTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method of
 * deleteItem when input parameter of id is invalid
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_DeleteItem_InvalidIdTest
extends Infrastructure_Test_Integration_Model_Crud_DeleteItem_InvalidIdTestCase
{
    /**
     * Stores the instance of authentication service
     * 
     * @var Authorization_Service_Authentication
     */
    protected $_authentication;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_authentication = new Authorization_Service_Authentication();
    }
    
    public function tearDown()
    {
        $this->getConnection()->getConnection()->exec('DELETE FROM users');
        
        parent::tearDown();
    }
    
    public function getDataSet()
    {
        return $this->createFlatXMLDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/models/User/'
            . 'DeleteItem/InvalidId.xml'
        );
    }
    
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getId()
    {
        return 'invalidId';
    }
    
    protected function _login()
    {
        return $this->_authentication->authenticate(
            array(
                'email'=>'admin@company.com',
                'password'=>'acdseeacdseeubot'
            )
        );
    }
    
    protected function _logout()
    {
        return $this->_authentication->clear();
    }
}
