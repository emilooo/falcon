<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Acl/GetIdentity/'
    . 'ExistIdentityTestCase.php';

/**
 * Implements the basic implementation for testing the behavior of method
 * of getIdentity when identity is correct
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_GetIdentity_ExistIdentityTest
extends Infrastructure_Test_Integration_Model_Acl_GetIdentity_ExistIdentityTestCase
{
    /**
     * Stores the instance of authentication
     * 
     * @var Authorization_Service_Authentication
     */
    private $_authentication;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_authentication = new Authorization_Service_Authentication();
    }
    
    public function getDataSet()
    {
        return $this->createFlatXMLDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/models/User/'
            . 'GetIdentity/ExistIdentity.xml'
        );
    }
    
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _loginUser()
    {
        $authenticationResult = $this->_authentication->authenticate(
            array(
                'email' => 'admin@company.com',
                'password' => 'acdseeacdseeubot'
            )
        );
        
        return $authenticationResult;
    }
    
    protected function _logoutUser()
    {
        return $this->_authentication->clear();
    }
}