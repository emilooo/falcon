<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/'
    . 'DatabaseTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method
 * of readOneItemByEmail when input parameter of email is correct and
 * exist in the structure of database
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_ReadOneItemByEmail_EmailExistTest
extends Infrastructure_Test_Integration_Model_DatabaseTestCase
{
    /**
     * Executes the method of readOneItemByEmail together with correct
     * name of email
     */
    public function test_readOneItemByEmail_InsertExistEmail_True()
    {
        $makeModelResult = $this->makeModel();
        $readOneItemByEmailResult = $makeModelResult->readOneItemByEmail(
            $this->_getEmail()
        );
        $isCorrectResult
            = ($readOneItemByEmailResult instanceof Zend_Db_Table_Row_Abstract);
        
        $this->assertTrue($isCorrectResult);
    }
    
    public function getDataSet()
    {
        return $this->createFlatXMLDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/models/User/'
            . 'ReadOneItemByEmail/EmailExist.xml'
        );
    }
    
    public function makeModel()
    {
        $userModel = new Authorization_Model_User();
        
        return $userModel;
    }
    
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    private function _getEmail()
    {
        return 'admin@company.com';
    }
}