<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the basic interface for testing the behavior of method
 * of readOneItemByEmail when input parameter of email is invalid
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_ReadOneItemByEmail_EmailNotExistTest
extends PHPUnit_Framework_TestCase
{
    public function test_readOneItemByEmail_InsertNotExistEmail_True()
    {
        try {
            $makeModelResult = $this->_makeModdel();
            $readOneItemByEmailResult = $makeModelResult->readOneItemByEmail(
                $this->_getEmail()
            );
        } catch (Infrastructure_Model_Resource_Db_Table_Row_NotExist $ex) {
            $isCorrectExceptionMessage
                = ($ex->getMessage() == 'Item not exist');
            
            return $this->assertTrue($isCorrectExceptionMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    protected function _makeModdel()
    {
        $userModel = new Authorization_Model_User();
        
        return $userModel;
    }
    
    private function _getEmail()
    {
        return 'notexist@company.com';
    }
}