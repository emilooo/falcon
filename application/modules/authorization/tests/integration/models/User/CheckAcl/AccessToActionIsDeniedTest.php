<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Acl/CheckAcl/'
    . 'AccessToActionIsDeniedTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method
 * hasn't access
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_CheckAcl_AccessToActionIsDeniedTest
extends Infrastructure_Test_Integration_Model_Acl_CheckAcl_AccessToActionIsDeniedTestCase
{
    /**
     * Stores the instance of authentication
     * 
     * @var Authorization_Service_Authentication
     */
    private $_authentication;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_authentication = new Authorization_Service_Authentication();
    }
    
    public function getDataSet()
    {
        return $this->createFlatXMLDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/models/User/'
            . 'CheckAcl/AccessToActionIsDenied.xml'
        );
    }
    
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getActionName()
    {
        return 'createItem';
    }
    
    protected function _login()
    {
        $authenticationResult = $this->_authentication->authenticate(
            array(
                'email' => 'admin@company.com',
                'password' => 'acdseeacdseeubot'
            )
        );
        
        return $authenticationResult;
    }
    
    protected function _logout()
    {
        return $this->_authentication->clear();
    }
}