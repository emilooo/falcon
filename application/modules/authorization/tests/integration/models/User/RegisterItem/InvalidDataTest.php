<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Auth/RegisterItem/'
    . 'InvalidDataTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method of
 * registerItem when input parameter of dataToRegister are incorrect
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_RegisterItem_InvalidDataTest
extends Infrastructure_Test_Integration_Model_Auth_RegisterItem_InvalidDataTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getDataToRegister()
    {
        return 'invalidDataToRegister';
    }
}