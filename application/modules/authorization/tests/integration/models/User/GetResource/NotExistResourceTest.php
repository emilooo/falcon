<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/GetResource/'
    . 'NotExistResourceTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method
 * of getResource when input parameter of resourceName is correct
 * but not exist in the structure of system
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_GetResource_NotExistResourceTest
extends Infrastructure_Test_Integration_Model_GetResource_NotExistResourceTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getResourceName()
    {
        return 'NotExist';
    }
}