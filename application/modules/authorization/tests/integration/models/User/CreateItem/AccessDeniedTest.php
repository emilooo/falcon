<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Crud/CreateItem/'
    . 'AccessDeniedTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method of
 * createItem when access to this method is denied
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_CreateItem_AccessDeniedTest
extends Infrastructure_Test_Integration_Model_Crud_CreateItem_AccessDeniedTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getDataToSave()
    {
        return array(
            'user_name'=>'Nobody',
            'user_surname'=>'Test'
        );
    }
}
