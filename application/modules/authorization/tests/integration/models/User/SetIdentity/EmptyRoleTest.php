<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Acl/SetIdentity/'
    . 'EmptyRoleTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method
 * of setIdentity when column of user_role is empty
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_SetIdentity_EmptyRoleTest
extends Infrastructure_Test_Integration_Model_Acl_SetIdentity_EmptyRoleTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getIdentity()
    {
        return new Authorization_Model_Resource_User_Row(
            array(
                'data'=>array(
                    'user_role' => null
                )
            )
        );
    }
}