<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Crud/UpdateItem/'
    . 'AccessDeniedTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method of
 * updateItem when access to this method is denied
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_UpdateItem_AccessDeniedTest
extends Infrastructure_Test_Integration_Model_Crud_UpdateItem_AccessDeniedTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getDataToUpdate()
    {
        return array(
            'name' => 'John',
            'password' => 'acdseeacdseeubot',
            'passwordrepeat' => 'acdseeacdseeubot'
        );
    }
    
    protected function _getId()
    {
        return 1;
    }
}
