<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Crud/UpdateItem/'
    . 'IncorrectDataToUpdateTestCase.php';

/**
 * Implements the basic interface for testing the behavior of method of
 * updateItem when input parameter of dataToUpdate is incorrect
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Integration
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Integration_Model_User_UpdateItem_IncorrectDataToUpdateTest
extends Infrastructure_Test_Integration_Model_Crud_UpdateItem_IncorrectDataToUpdateTestCase
{
    /**
     * Stores the instance of authentication service
     * 
     * @var Authorization_Service_Authentication
     */
    protected $_authentication;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_authentication = new Authorization_Service_Authentication();
    }
    
    public function tearDown()
    {
        $this->getConnection()->getConnection()->exec('DELETE FROM users');
        
        parent::tearDown();
    }
    
    public function getDataSet()
    {
        return $this->createFlatXMLDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/models/User/'
            . 'UpdateItem/IncorrectDataToUpdate.xml'
        );
    }
    
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getDataToUpdate()
    {
        return array(
            'lastname' => 1,
            'password' => 'acdseeacdseeubot',
            'passwordrepeat' => 'acdseeacdseeubot'
        );
    }
    
    protected function _getId()
    {
        return 1;
    }
    
    protected function _login()
    {
        return $this->_authentication->authenticate(
            array(
                'email'=>'admin@company.com',
                'password'=>'acdseeacdseeubot'
            )
        );
    }
    
    protected function _logout()
    {
        return $this->_authentication->clear();
    }
    
    protected function _getActualTable()
    {
        return $this->getConnection()->createQueryTable(
            "users",
            "SELECT user_id, user_login, user_firstname, user_lastname,"
            . "user_email, user_password, user_salt, user_role, user_status FROM users"
        );
    }
    
    protected function _getExpectedTable()
    {
        return $this->createFlatXMLDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/models/User/'
            . 'UpdateItem/IncorrectDataToUpdate.xml'
        )->getTable('users');
    }
}
