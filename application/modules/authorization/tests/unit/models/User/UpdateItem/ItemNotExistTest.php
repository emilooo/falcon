<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/UpdateItem/'
    . 'ItemNotExistTestCase.php';

/**
 * Implements the basic implementation for testing the behavior of method
 * of updateItem when input parameter of dataToUpdate is correct
 * and resource not exist
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_UpdateItem_ItemNotExistTest
extends Infrastructure_Test_Unit_Model_Crud_UpdateItem_ItemNotExistTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getId()
    {
        return 1;
    }
    
    protected function _getDataToUpdate()
    {
        return array(
            'name'=>'John',
            'password'=>'acdseeacdseeubot',
            'passwordrepeat'=>'acdseeacdseeubot'
        );
    }
}