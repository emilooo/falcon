<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/CreateItem/'
    . 'CorrectDataToSaveTestCase.php';

/**
 * Implements the basic implementation for testing the behavior of method
 * of createItem when input parameter of dataToSave is correct
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_CreateItem_CorrectDataToSaveTest
extends Infrastructure_Test_Unit_Model_Crud_CreateItem_CorrectDataToSaveTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getCorrectDataToSave()
    {
        return array(
            'login'=>'Test',
            'firstname'=>'Test',
            'lastname'=>'Test',
            'email'=>'admin@company.com',
            'password'=>'test',
            'salt'=>'test',
            'role'=>'User'
        );
    }
}