<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/SetProperties/'
    . 'CorrectPropertiesTestCase.php';

/**
 * Implements the basic implementation for testing the behavior of method
 * of setOptions when input parameter of options is correct
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_SetProperties_CorrectPropertiesTest
extends Infrastructure_Test_Unit_Model_SetProperties_CorrectPropertiesTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getOptions()
    {
        return array(
            'name',
            'surname'
        );
    }
}