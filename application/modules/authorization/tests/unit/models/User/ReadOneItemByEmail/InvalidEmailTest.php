<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Testing the behavior of method of readOneItemByEmail when try gets the
 * row through invalid input parameter of email
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_ReadOneItemByEmail_InvalidEmailTest
extends PHPUnit_Framework_TestCase
{
    /**
     * Executes the method of readOneItemByEmail together with
     * invalid input parameter of email
     */
    public function test_readOneItemByEmail_InvalidEmail_Exception()
    {
        try {
            $makeModelResult = $this->_makeModel();
            $makeModelResult->readOneItemByEmail($this->_getEmail());
        } catch (Exception $ex) {
            $isCorrectMessage
                = ($ex->getMessage() == 'assert(): Assertion failed');
            
            return $this->assertTrue($isCorrectMessage);
        }
        
        $this->fail('An expected exception has not been raised.');
    }
    
    protected function _makeModel()
    {
        $userModel = new Authorization_Model_User();
        
        return $userModel;
    }
    
    protected function _getEmail()
    {
        return 1;
    }
}