<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Testing the behavior of method of readOneItemByEmail when try gets the
 * row through correct and exist input parameter of email
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_ReadOneItemByEmail_EmailExistTest
extends PHPUnit_Framework_TestCase
{
    /**
     * Executes the method of readOneItemByEmail together with
     * invalid input parameter of email
     */
    public function test_readOneItemByEmail_ExistEmail_True()
    {
        $makeModelResult = $this->_makeModel();
        $readOneItemByEmailResutl = $makeModelResult
            ->readOneItemByEmail($this->_getEmail());
        $isCorrectRow
            = ($readOneItemByEmailResutl instanceof Zend_Db_Table_Row);

        $this->assertTrue($isCorrectRow);
    }
    
    protected function _makeModel()
    {
        $userModel = new Authorization_Model_User(
            array(
                'acl' => $this->_makeAclMock(),
                'resources' => array(
                    'User' => $this->_makeResourceMock()
                )
            )
        );
        
        return $userModel;
    }
    
    protected function _getEmail()
    {
        return 'admin@company.com';
    }
    
    private function _makeResourceMock()
    {
        $resourceMock
            = Mockery::mock(
                'Infrastructure_Model_Resource_Db_Table_Crud_Interface'
            );
        $resourceMock
            ->shouldReceive('readOneItem')
            ->with('user_email', 'admin@company.com')
            ->andReturn($this->_makeRowMock());
        
        return $resourceMock;
    }
    
    private function _makeRowMock()
    {
        $rowMock = Mockery::mock('Zend_Db_Table_Row');
        
        return $rowMock;
    }
    
    /**
     * Returns instance of alc mock
     * 
     * @return Zend_Acl
     */
    private function _makeAclMock()
    {
        $aclMock = Mockery::mock('Zend_Acl');
        $aclMock
            ->shouldReceive('isAllowed')
            ->andReturn(true);
        
        return $aclMock;
    }
}