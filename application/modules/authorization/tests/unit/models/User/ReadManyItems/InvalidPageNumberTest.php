<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/ReadManyItems/'
    . 'InvalidPageNumberTestCase.php';

/**
 * Testing the behavior of method of readManyItems when input parameter
 * of pageNumber is invalid
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_ReadManyItems_InvalidPageNumberTest
extends Infrastructure_Test_Unit_Model_Crud_ReadManyItems_InvalidPageNumberTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getInvalidPageNumber()
    {
        return null;
    }
}