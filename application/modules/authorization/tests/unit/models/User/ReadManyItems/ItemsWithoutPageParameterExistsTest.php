<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/ReadManyItems/'
    . 'ItemsWithoutPageParameterExistsTestCase.php';

/**
 * Testing the behavior of method of readManyItems when input parameter
 * of pageNumber is empty
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_ReadManyItems_ItemsWithoutPageParameterExistsTest
extends Infrastructure_Test_Unit_Model_Crud_ReadManyItems_ItemsWithoutPageParameterExistsTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getCorrectPageNumber()
    {
        return null;
    }
}