<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Crud/DeleteItem/'
    . 'ItemNotExistTestCase.php';

/**
 * Implements the basic implementation for testing the behavior of method
 * of deleteItem when input parameter of id is correct but item not exist
 * 
 * @abstract
 * @category Test
 * @package Test_Authorization_Unit
 * @subpackage Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Unit_UserModel_DeleteItem_ItemNotExistTest
extends Infrastructure_Test_Unit_Model_Crud_DeleteItem_ItemNotExistTestCase
{
    protected function _getModelFullClassName()
    {
        return 'Authorization_Model_User';
    }
    
    protected function _getModelShortClassName()
    {
        return 'User';
    }
    
    protected function _getId()
    {
        return 1;
    }
}