<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Http/Auth/LoginAction/'
    . 'UserNotExistTestCase.php';

/**
 * Testing the behavior of method of loginAction when data of post
 * have content for not exist user
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_LoginAction_UserNotExistTest
extends Infrastructure_Test_Functional_Controller_Http_Auth_LoginAction_UserNotExistTestCase
{
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'login';
    }
    
    protected function _getPost()
    {
        return array(
            'email' => 'admin@company.com',
            'password' => 'acdseeacdseeubot'
        );
    }
}