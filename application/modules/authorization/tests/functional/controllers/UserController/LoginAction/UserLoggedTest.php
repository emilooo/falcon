<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Http/Auth/LoginAction/'
    . 'UserLoggedTestCase.php';

/**
 * Testing the behavior of method of loginAction when user is logged
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_LoginAction_UserLoggedTest
extends Infrastructure_Test_Functional_Controller_Http_Auth_LoginAction_UserLoggedTestCase
{
    /**
     * Stores the instance of authentication service
     * 
     * @var Authorization_Service_Authentication
     */
    protected $_authentication;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_authentication = new Authorization_Service_Authentication();
    }
    
    public function getDataSet()
    {
        return new PHPUnit_Extensions_Database_DataSet_FlatXmlDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/controllers/UserController/'
            . 'LoginAction/UserLoggedFixtureBefore.xml'
        );
    }
    
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'login';
    }
    
    protected function _getPost()
    {
        return array(
            'email' => 'admin@company.com',
            'password' => 'acdseeacdseeubot'
        );
    }
    
    protected function _login()
    {
        return $this->_authentication->authenticate(
            array(
                'email' => 'admin@company.com',
                'password' => 'acdseeacdseeubot'
            )
        );
    }
    
    protected function _logout()
    {
        return $this->_authentication->clear();
    }
}