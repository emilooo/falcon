<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once APPLICATION_PATH
    . 'modules/authorization/tests/functional/controllers/UserController/'
     . 'RegisterAction/UnauthorizedRequestTestCase.php';

/**
 * Testing the behavior of method of registerAction when token is unauthorized
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_RegisterAction_UnauthorizedRequestTest
extends Authorization_Test_Functional_UserController_RegisterAction_UnauthorizedRequestTestCase
{
    public function setUp()
    {
        parent::setUp();
        
        $this->markTestSkipped(
            "At the moment this authorization of request is uncompatible."
        );
    }
    
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'register';
    }
    
    protected function _getPost()
    {
        return array(
            'firstname' => 'John',
            'lastname' => 'Dee',
            'login' => 'john.dee',
            'email' => 'john.dee@example.com',
            'password' => 'testtest',
            'passwordrepeat' => 'testtest'
        );
    }
    
    protected function _getToken()
    {
        return 'test';
    }
}