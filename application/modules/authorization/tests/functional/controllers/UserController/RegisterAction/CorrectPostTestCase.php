<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Http/Auth/RegisterAction/'
    . 'CorrectPostTestCase.php';

/**
 * Testing the behavior of method of registerAction when data of post are
 * correct
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_RegisterAction_CorrectPostTestCase
extends Infrastructure_Test_Functional_Controller_Http_Auth_RegisterAction_CorrectPostTestCase
{
    public function getDataSet()
    {
    }
    
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'register';
    }
    
    protected function _getPost()
    {
        return array(
            'firstname' => 'John',
            'lastname' => 'Dee',
            'login' => 'john.dee',
            'email' => 'john.dee@example.com',
            'password' => 'testtest',
            'passwordrepeat' => 'testtest'
        );
    }
}