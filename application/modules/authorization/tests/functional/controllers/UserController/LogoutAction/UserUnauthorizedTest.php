<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Http/Auth/LogoutAction/'
    . 'UserUnauthorizedTestCase.php';

/**
 * Testing the behavior of method of loginAction when user is logged
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_LogoutAction_UserUnauthorizedTest
extends Infrastructure_Test_Functional_Controller_Http_Auth_LogoutAction_UserUnauthorizedTestCase
{
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'logout';
    }
    
    protected function _getLoginUrl()
    {
        return '/en/login';
    }
}