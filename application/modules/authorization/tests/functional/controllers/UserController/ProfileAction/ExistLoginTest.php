<?php
/**
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Http/Auth/ProfileAction/'
    . 'AccessDeniedToActionTestCase.php';

/**
 * This test verifing the behavior of action of profile when
 * try gets the information about user profile through exist login
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_ProfileAction_ExistLoginTest
extends Infrastructure_Test_Functional_Controller_Http_Auth_ProfileAction_ExistLoginTestCase
{
    /**
     * Stores the instance of authentication service
     * 
     * @var Authorization_Service_Authentication
     */
    protected $_authentication;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_authentication = new Authorization_Service_Authentication();
    }
    
    public function getDataSet()
    {
        return new PHPUnit_Extensions_Database_DataSet_FlatXmlDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/controllers/UserController/'
            . 'ProfileAction/ExistLoginFixtureBefore.xml'
        );
    }
    
    public function tearDown()
    {
        parent::tearDown();
        
        $this->getConnection()->getConnection()->exec('DELETE FROM users');
    }
    
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'profile';
    }
    
    protected function _getLogin() {
        return 'user';
    }
    
    protected function _login()
    {
        $authenticateResult = $this->_authentication->authenticate(
            array(
                'email'=>'admin@company.com',
                'password'=>'acdseeacdseeubot'
            )
        );
        
        return $authenticateResult;
    }
    
    protected function _logout()
    {
        $clearResult = $this->_authentication->clear();
        
        return $clearResult;
    }
}