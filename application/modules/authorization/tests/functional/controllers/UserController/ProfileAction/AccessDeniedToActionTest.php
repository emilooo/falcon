<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Http/Auth/ProfileAction/'
    . 'AccessDeniedToActionTestCase.php';

/**
 * This test verifing the behavior of action of index when
 * try updates but access to action is denied
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_ProfileAction_AccessDeniedToActionTest
extends Infrastructure_Test_Functional_Controller_Http_Auth_ProfileAction_AccessDeniedToActionTestCase
{
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'profile';
    }
    
    protected function _getLogin() {
        return 'testuser';
    }
}