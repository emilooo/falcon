<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Functional/Controller/Http/Crud/DeleteAction/'
    . 'EmptyIdTestCase.php';

/**
 * Implements the basic interface for testing the behavior
 * of action of deleteAction when id of item not set
 * 
 * @category Test
 * @package Test_Authorization_Functional_Controller
 * @subpackage User
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Authorization_Test_Functional_UserController_DeleteAction_EmptyIdIdTest
extends Infrastructure_Test_Functional_Controller_Http_Crud_DeleteAction_EmptyIdTestCase
{
    /**
     * Stores the instance of authentication service
     * 
     * @var Authorization_Service_Authentication
     */
    protected $_authentication;
    
    public function setUp()
    {
        parent::setUp();
        
        $this->_authentication = new Authorization_Service_Authentication();
    }
    
    public function getDataSet()
    {
        return new PHPUnit_Extensions_Database_DataSet_FlatXmlDataSet(
            APPLICATION_PATH . 'modules/authorization/tests/fixture/controllers/UserController/'
            . 'DeleteAction/EmptyIdFixtureBefore.xml'
        );
    }
    
    public function tearDown()
    {
        parent::tearDown();
        
        $this->getConnection()->getConnection()->exec('DELETE FROM users');
    }
    
    protected function _getModuleName()
    {
        return 'authorization';
    }

    protected function _getControllerName()
    {
        return 'user';
    }
    
    protected function _getActionName()
    {
        return 'delete';
    }
    
    protected function _login()
    {
        $authenticateResult = $this->_authentication->authenticate(
            array(
                'email'=>'admin@company.com',
                'password'=>'acdseeacdseeubot'
            )
        );
        
        return $authenticateResult;
    }
    
    protected function _logout()
    {
        $clearResult = $this->_authentication->clear();
        
        return $clearResult;
    }
}