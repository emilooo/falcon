<?php
// CONTROLLERS

// Translation for messages from user controller 
$polishTranslation['MessageForCorrectLoginProcess'] = 'Proces logowania zakończył się powodzeniem';
$polishTranslation['MessageForInvalidLoginProcess'] = 'Proces logowania zakończył się niepowodzeniem!';
$polishTranslation['MessageForCorrectLogoutProcess'] = 'Proces wylogowania zakończył się powodzeniem';
$polishTranslation['MessageForInvalidLogoutProcess'] = 'Proces wylogowania zakończył się niepowodzeniem';
$polishTranslation['MessageForCorrectRegisterProcess'] = 'Proces rejestracji zakończył się powodzeniem';
$polishTranslation['MessageForInvalidRegisterProcess'] = 'Proces rejestracji zakończył się niepowodzeniem!';
$polishTranslation['MessageForInvalidRegisterProcess'] = 'Proces rejestracji zakończył się niepowodzeniem!';
$polishTranslation['MessageForCorrectCreateProcess'] = 'Operacja dodawania nowej pozycji została zakończona powodzeniem!';
$polishTranslation['MessageForInvalidCreateProcess'] = 'Operacja dodawania nowej pozycji została zakończona niepowodzeniem!';
$polishTranslation['MessageForCorrectDeleteProcess'] = 'Operacja kasowania pozycji została zakończona powodzeniem!';
$polishTranslation['MessageForInvalidDeleteProcess'] = 'Operacja kasowania pozycji została zakończona niepowodzeniem!';
$polishTranslation['MessageForCorrectUpdateProcess'] = 'Operacja aktualizacji pozycji została zakończona powodzeniem!';
$polishTranslation['MessageForInvalidUpdateProcess'] = 'Operacja aktualizacji pozycji została zakończona niepowodzeniem!';

// TEMPLATES
// Translation for admin template
$polishTranslation['Show the user profile'] = 'Pokaż profil użytkownika';
$polishTranslation['Profile'] = 'Profil';
$polishTranslation['Log out user'] = 'Wyloguj użytkownika';
$polishTranslation['Log out'] = 'Wyloguj';
$polishTranslation['Logged in us'] = 'Zalogowany jako';

// VIEWS
// Translation for index view file from controller of user inside of authorization module
$polishTranslation['MessageForEmptyResults'] = 'Brak wyników do wyświetlenia';
$polishTranslation['List of results'] = 'Lista wyników';
$polishTranslation['Edit record'] = 'Edytuj rekord';
$polishTranslation['Remove record'] = 'Usuń rekord';
$polishTranslation['Go to the previous page'] = 'Przejdź do poprzedniej strony';
$polishTranslation['Go to the next page'] = 'Przejdź do następnej strony';
$polishTranslation['Messages'] = 'Komunikaty';
$polishTranslation['Refresh results'] = 'Odśwież wyniki';
$polishTranslation['Check all records'] = 'Zaznacz wszystkie rekordy';
$polishTranslation['Remove selected records'] = 'Usuń zaznaczone rekordy';
$polishTranslation['Add new record'] = 'Dodaj nowy rekord';

// VIEW FILES
// Translation for view files from user controller inside of core module
$polishTranslation['Users'] = 'Użytkownicy';
$polishTranslation['Select role: '] = 'Wybierz grupę: ';
$polishTranslation['Select status: '] = 'Wybierz status: ';
$polishTranslation['Insert the firstname of user'] = 'Wprowadź imię użytkownika';
$polishTranslation['Insert the lastname of user'] = 'Wprowadź nazwisko użytkownika';
$polishTranslation['Insert the login of user'] = 'Wprowadź login użytkownika';
$polishTranslation['Insert the e-mail of user'] = 'Wprowadź adres e-mail użytkownika';
$polishTranslation['Select the group of user'] = 'Wybierz grupę użytkownika';
$polishTranslation['Insert the password of user'] = 'Wprowadź hasło użytkownika';
$polishTranslation['Repeat the password of user'] = 'Wprowadź ponownie hasło użytkownika';
$polishTranslation['Select the status of user'] = 'Wybierz status użytkownika';
$polishTranslation['LoginUserViewHeader'] = 'Logowanie';
$polishTranslation['Guest'] = 'Gość';
$polishTranslation['User'] = 'Użytkownik';
$polishTranslation['Admin'] = 'Administrator';
$polishTranslation['Unconfirmed'] = 'Niepotwierdzony';
$polishTranslation['Inactive'] = 'Nieaktywny';
$polishTranslation['Active'] = 'Aktywny';

// Translation for view files from user controller and register action
$polishTranslation['RegisterUserViewHeader'] = 'Rejestracja';
$polishTranslation['Register'] = 'Rejestracja';

// Translation for view files from user controller and login action
$polishTranslation['LinkToLoginUserView'] = 'Zaloguj użytkownika';
$polishTranslation['LinkToRegisterUserView'] = 'Zarejestruj użytkownika';
$polishTranslation['Login'] = 'Zaloguj';

// FORM LABELS
// Translation for user form labels
$polishTranslation['UserFormLoginLabel:'] = 'Login użytkownika:';
$polishTranslation['UserFormFirstnameLabel:'] = "Imię użytkownika:";
$polishTranslation['UserFormLastnameLabel:'] = 'Nazwisko użytkownika:';
$polishTranslation['UserFormEmailLabel:'] = 'Adres e-mail użytkownika:';
$polishTranslation['UserFormPasswordLabel:'] = 'Hasło użytkownika:';
$polishTranslation['UserFormPasswordrepeatLabel:'] = 'Powtórz hasło użytkownika:';
$polishTranslation['UserFormRegisterLabel'] = 'Zarejestruj';
$polishTranslation['UserFormLoginLabel'] = 'Zaloguj';
$polishTranslation['UserFormRoleLabel: '] = 'Grupa użytkownika: ';
$polishTranslation["UserFormStatusLabel: "] = "Status użytkownika: ";

return $polishTranslation;
