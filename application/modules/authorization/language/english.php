<?php

// CONTROLLERS
// Translation for messages from user controller 
$englishTranslation['MessageForCorrectLoginProcess'] = 'Process of login was successfully ended!';
$englishTranslation['MessageForInvalidLoginProcess'] = 'Process of login was not successfully ended!';
$englishTranslation['MessageForCorrectLogoutProcess'] = 'Process of logout was successfully ended!';
$englishTranslation['MessageForInvalidLogoutProcess'] = 'Process of logout was not successfully ended!';
$englishTranslation['MessageForCorrectRegisterProcess'] = 'Process of register was successfully ended!';
$englishTranslation['MessageForInvalidRegisterProcess'] = 'Process of register was not successfully ended!';
$englishTranslation['MessageForCorrectCreateProcess'] = 'The operation of add new item was successfully ended!';
$englishTranslation['MessageForInvalidCreateProcess'] = 'The operation of add new item was not successfully ended!';
$englishTranslation['MessageForCorrectDeleteProcess'] = 'The operation of remove existing item was successfully ended!';
$englishTranslation['MessageForInvalidDeleteProcess'] = 'The operation of remove existing item was not successfully ended!';
$englishTranslation['MessageForCorrectUpdateProcess'] = 'The operation of update existing item was successfully ended!';
$englishTranslation['MessageForInvalidUpdateProcess'] = 'The operation of update existing item was not successfully ended!';

// VIEW FILES
// Translation for view files from user controller inside of core module
$englishTranslation['Users'] = 'Users';
$englishTranslation['Select role: '] = 'Select group: ';
$englishTranslation['Insert the firstname of user'] = 'Insert the name of user';
$englishTranslation['Insert the lastname of user'] = 'Insert the surname of user';
$englishTranslation['Insert the login of user'] = 'Insert the login of user';
$englishTranslation['Insert the e-mail of user'] = 'Insert the e-mail address of user';
$englishTranslation['Insert the group of user'] = 'Select the group of user';
$englishTranslation['Insert the password of user'] = 'Insert the password of user';
$englishTranslation['Repeat the password of user'] = 'Repeat the password of user';
$englishTranslation['LoginUserViewHeader'] = 'Login';
$englishTranslation['Guest'] = 'Guest';
$englishTranslation['User'] = 'User';
$englishTranslation['Admin'] = 'Admin';

// Translation for view files from user controller and register action
$englishTranslation['RegisterUserViewHeader'] = 'Register';
$englishTranslation['Register'] = 'Register';

// Translation for view files from user controller and login action
$englishTranslation['LinkToLoginUserView'] = 'Login user';
$englishTranslation['LinkToRegisterUserView'] = 'Register user';
$englishTranslation['Login'] = 'Login';


// FORM LABELS
// Translation for user form labels
$englishTranslation['UserFormLoginLabel:'] = 'Login:';
$englishTranslation['UserFormFirstnameLabel:'] = "Name:";
$englishTranslation['UserFormLastnameLabel:'] = 'Surname:';
$englishTranslation['UserFormEmailLabel:'] = 'E-mail address:';
$englishTranslation['UserFormPasswordLabel:'] = 'Password:';
$englishTranslation['UserFormPasswordrepeatLabel:'] = 'Repeat password:';
$englishTranslation['UserFormRegisterLabel'] = 'Register';
$englishTranslation['UserFormLoginLabel'] = 'Login';
$englishTranslation['UserFormRoleLabel: '] = 'Group of user: ';
$englishTranslation["UserFormStatusLabel: "] = "Status of user: ";

return $englishTranslation;