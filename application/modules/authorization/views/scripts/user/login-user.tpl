<header class="row">
    <div class='col-lg-12 text-center'>
        <img
            src='/img/application/templates/login/logo.png' 
            title='Logotype'
            class='logotype'
        />
    </div>

    <nav class='col-lg-12'>
        <h1>
            <a href=''>{$this->translate('LoginUserViewHeader')}</a>
        </h1>
    </nav>
</header>

<main>
    <ul id='messages'>
    {if (count($this->messages) > 0)}
        {foreach from=$this->messages item="messageValue" key="messageKey"}
            <li>{$this->translate($messageValue)}</li>
        {/foreach}
    {/if}
    </ul>
    
    {$this->form}
    
    <ul style="text-align:right;">
        <li><a href='{$this->url(array(), 'registerUser')}'>{$this->translate('LinkToRegisterUserView')}</a></li>
    </ul>
</main>