<header class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-7 col-sm-8 col-md-8 col-lg-8">
        <div class="hidden-xs hidden-sm col-md-1 col-lg-1">
            <div id="moduleIcon"></div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
            <h1>{$this->translate('Users')}</h1>
            
            {$this->navigation()->breadcrumbs()->setMinDepth(0)->setSeparator(' &raquo; ')}

            <p class="hidden-xs">Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym.</p>
        </div>
    </div>

    <div class="col-xs-5 col-sm-4 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
        <ul>
            <li class="active"><a href="{$this->url(array(), 'createUser', true)}" title="{$this->translate('Add new record')}" id="createRecord">{$this->translate('Add new record')}</a></li>
        </ul>

        <ul>
            <li><a href="{$this->url()}" title="{$this->translate('Refresh results')}" id="refreshResults">{$this->translate('Refresh results')}</a></li>
        </ul>
    </div>
</header>

<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="list-records">
    <header class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <nav class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul>
                <li><a href="{$this->url(array(), 'listUsers')}" title="{$this->translate('Go to the previous page')}" id="backPageIcon">{$this->translate('Go to the previous page')}</a></li>
            </ul>
        </nav>

        <h2>{$this->translate('Profile')}</h2>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="messages">
            {if (count($this->messages) > 0)}
            <p>Messages: </p>
            <ul>
                {foreach from=$this->messages item="messageValue" key="messageKey"}
                    <li>{$this->translate($messageValue)}</li>
                {/foreach}
            </ul>
            {/if}
        </div>
    </header>
</section>

<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="show-profile">
    <dl class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <dt class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
            {$this->translate("UserFormLoginLabel:")}
        </dt>
        <dd class="col-xs-12 col-sm-9 col-md-9 col-lg-8">
            {trim($this->record->user_login)}
        </dd>

        <dt class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
            {$this->translate("UserFormFirstnameLabel:")}
        </dt>
        <dd class="col-xs-12 col-sm-9 col-md-9 col-lg-8">
            {trim($this->record->user_firstname)}
        </dd>

        <dt class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
            {$this->translate("UserFormLastnameLabel:")}
        </dt>
        <dd class="col-xs-12 col-sm-9 col-md-9 col-lg-8">
            {trim($this->record->user_lastname)}
        </dd>

        <dt class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
            {$this->translate("UserFormRoleLabel: ")}
        </dt>
        <dd class="col-xs-12 col-sm-9 col-md-9 col-lg-8">
            {$this->translate($this->record->user_role)}
        </dd>

        <dt class="col-xs-12 col-sm-3 col-md-3 col-lg-4">
            {$this->translate("UserFormStatusLabel: ")}
        </dt>
        <dd class="col-xs-12 col-sm-9 col-md-9 col-lg-8">
            {$this->translate($this->record->user_status)}
        </dd>
    </dl>
</section>