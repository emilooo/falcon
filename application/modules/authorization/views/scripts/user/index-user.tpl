<header class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
    <div class="col-xs-7 col-sm-8 col-md-8 col-lg-8">
        <div class="hidden-xs hidden-sm col-md-1 col-lg-1">
            <div id="moduleIcon"></div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-11 col-lg-11">
            <h1>{$this->translate('Users')}</h1>
            
            {$this->navigation()->breadcrumbs()->setMinDepth(0)->setSeparator(' &raquo; ')}

            <p class="hidden-xs">Lorem Ipsum jest tekstem stosowanym jako przykładowy wypełniacz w przemyśle poligraficznym. Został po raz pierwszy użyty w XV w. przez nieznanego drukarza do wypełnienia tekstem próbnej książki. Pięć wieków później zaczął być używany przemyśle elektronicznym, pozostając praktycznie niezmienionym.</p>
        </div>
    </div>

    <div class="col-xs-5 col-sm-4 col-md-3 col-lg-3 col-md-offset-1 col-lg-offset-1">
        <ul>
            <li class="active"><a href="{$this->url(array(), 'createUser', true)}" title="{$this->translate('Add new record')}" id="createRecord">{$this->translate('Add new record')}</a></li>
        </ul>

        <ul>
            <li><a href="{$this->url(array(), 'listUsers', true)}" title="{$this->translate('Refresh results')}" id="refreshResults">{$this->translate('Refresh results')}</a></li>
            <li><a href="#" title="{$this->translate('Check all records')}" id="checkAllResults">{$this->translate('Check all records')}</a></li>
            <li><a href="#" title="{$this->translate('Remove selected records')}" id="removeAllCheckedResults">{$this->translate('Remove selected records')}</a></li>
        </ul>
    </div>
</header>

<section class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="list-records">
    <header class="col-lg-12">
        <nav class="col-lg-12">
            <ul>
                {$this->paginationControl($this->records, 'Sliding', 'paginationTemplate.tpl')}
            </ul>
        </nav>
        
        <h2>{$this->translate('List of results')}</h2>
        
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" id="messages">
            {if (count($this->messages) > 0 || $this->records->count() == 0)}
            <p>{$this->translate('Messages')}: </p>
            <ul>
                {foreach from=$this->messages item="messageValue" key="messageKey"}
                    <li>{$this->translate($messageValue)}</li>
                {/foreach}
                
                {if ($this->records->count() == 0)}
                    <li>{$this->translate('MessageForEmptyResults')}</li>
                {/if}
            </ul>
            {/if}
        </div>
    </header>
    
    {if ($this->records->count() > 0)}
        {foreach from=$this->records item="itemValue" key="itemKey"}
            <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div class="col-xs-8 col-sm-8 col-md-8 col-lg-8">
                    <ul>
                        <li>
                            <input type="checkbox" name="id" value="{$itemValue['user_id']}"/>
                        </li>
                        
                        <li>
                            <p>{$this->translate({$itemValue['user_firstname']})}</p>
                        </li>
                    </ul>
                </div>
                
                <div class=" col-lg-4">
                    <ul>
                        {$urlOption['id']=$itemValue['user_id']}
                        {$urlOption['login']=$itemValue['user_login']}
                        <li><a href="{$this->url($urlOption, 'profileUser', true)}" title="{$this->translate('Show the user profile')}" id="showProfile">{$this->translate('Show the user profile')}</a></li>
                        <li><a href="{$this->url($urlOption, 'deleteUser', true)}" title="{$this->translate('Remove record')}" id="deleteRecord">{$this->translate('Remove record')}</a></li>
                    </ul>
                </div>
            </div>
        {/foreach}
    {/if}
    
    <footer class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <nav class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <ul>
                {$this->paginationControl($this->records, 'Sliding', 'paginationTemplate.tpl')}
            </ul>
        </nav>
    </footer>
</section>