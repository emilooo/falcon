{if ($this->pageCount)}
<!-- Previous page link -->
    {if (isset($this->previous))}
        {$urlOption['page']=$this->previous}
        <li><a href="{$this->url($urlOption, null, true)}" title="{$this->translate('Go to the previous page')}" id="backPageIcon">{$this->translate('Go to the previous page')}</a></li>
    {else}
        <li><a href="" title="{$this->translate('Go to the previous page')}" id="backPageIcon">{$this->translate('Go to the previous page')}</a></li>
    {/if}
 
<!-- Next page link -->
    {if (isset($this->next))}
        {$urlOption['page']=$this->next}
        <li><a href="{$this->url($urlOption, null, true)}" title="{$this->translate('Go to the next page')}" id="nextPageIcon">{$this->translate('Go to the next page')}</a></li>
    {else}
        <li><a href="" title="{$this->translate('Go to the next page')}" id="nextPageIcon">{$this->translate('Go to the next page')}</a></li>
    {/if}
{/if}