<?php
$polishTranslation = array();

// CONTROLLERS
// Translation for error messages from error controller
$polishTranslation['MessageForAccessDeniedContentHeader'] = 'Dostęp zabroniony';
$polishTranslation['MessageForAccessDeniedContentMessage'] = 'Nie masz praw dostępu do zawartości strony';
$polishTranslation['MessageForPageNotFoundContentHeader'] = 'Strona nie istnieje';
$polishTranslation['MessageForPageNotFoundContentMessage'] = 'Strona o wskazanym adresie nie istnieje';
$polishTranslation['MessageForApplicationErrorContentHeader'] = 'Błąd aplikacji';
$polishTranslation['MessageForApplicationErrorContentMessage'] = 'Przepraszamy, wystąpił niespodziewany błąd aplikacji';
$polishTranslation['MessageForBuldingPageContentHeader'] = "Usługa w budowie";
$polishTranslation['MessageForBuldingPageContentMessage'] = 'Przepraszamy, usługa nie jest gotowa w tym momencie zapraszamy ponownie za jakiś czas.';
$polishTranslation['You haven\'t privileges to this action']
    = 'Nie posiadasz uprawnień do wykonania tej akcji';

// FORM VALIDATORS
$polishTranslation['emailAddressInvalid'] = 'Adres e-mail %value% jest nieprawidłowy!';
$polishTranslation['emailAddressInvalidHostname'] = '\'%hostname%\' nie jest poprawną nazwą domeny dla adresu e-mail \'%value%\'';
$polishTranslation['emailAddressInvalidFormat'] = '\'%value%\' nie jest poprawnym adresem e-mail. Adres e-mail powinien mieć postać part@hostname';
$polishTranslation['emailAddressInvalidMxRecord'] = "'%hostname%' nie zawiera poprawnego rekordu MX dla adresu '%value%'";
$polishTranslation['emailAddressInvalidSegment'] = "'%hostname%' nie jest w segmencie sieci rutowalnej. Adres e-mail '%value%' nie powinien być rozwiązywany przez publiczną sieć";
$polishTranslation['emailAddressDotAtom'] = "'%localPart%' can not be matched against dot-atom format";
$polishTranslation['emailAddressQuotedString'] = "'%localPart%' can not be matched against quoted-string format";
$polishTranslation['emailAddressInvalidLocalPart'] = "'%localPart%' nie jest poprawną częścią adresu e-mail '%value%'";
$polishTranslation['emailAddressLengthExceeded'] = "'%value%' przekracza dopuszczalną długość";
$polishTranslation['alnumInvalid'] = "Podano niepoprawny typ wartości. Wymagany, integer lub float";
$polishTranslation['notAlnum'] = "'%value%' zawiera znaki, które nie są literami ani liczbami";
$polishTranslation["alnumStringEmpty"] = "'%value%' jest pustym ciągiem";
$polishTranslation['stringLengthInvalid'] = "Podano niepoprawny typ wartości. Wymagany ciąg tekstowy";
$polishTranslation["stringLengthTooShort"] = "'%value%' zawiera za mało znaków. Wymagane mininum to %min% znaki";
$polishTranslation["stringLengthTooLong"] = "'%value%' zawiera zbyt dużo znaków. Wymagane maksimum to %max% znaki";
$polishTranslation['isEmpty'] = 'Pole jest puste!';
$polishTranslation['emailExists'] = 'Adres e-mail %value% już istnieje!';
$polishTranslation['loginExists'] = 'Identyfikator %value% już istnieje!';
$polishTranslation['notMatch'] = 'Hasła nie zgadzają się!';
$polishTranslation['urlIncorrect'] = 'Niepoprawny adres url. Poprawny powinien mieć następującą postać /example';
$polishTranslation['itemNotExist'] = '%value% nie istnieje w systemie!';
$polishTranslation['itemExist'] = '%value% istnieje w systemie!';
$polishTranslation['urlExists'] = 'Url %value% istnieje w systemie!';
$polishTranslation['valueExistsInDatabase'] = 'Wartość %value% już istnieje w systemie';

// TEMPLATES

//default template
$polishTranslation['Copright by'] = "Prawo autorskie zastrzeżone przez";
$polishTranslation['All right reserved'] = "Wszystkie prawa zastrzeżone";
$polishTranslation['Powered by'] = "Wspierany przez";
$polishTranslation["In order to ensure maximum convenience to users when using the website, this page uses cookie files. Detailed information is available in our Privacy Policy. Click \"I agree\", so that this information is no longer shown."] = "W celu zapewnienia maksymalnej wygody użytkowników przy korzystaniu z witryny ta strona stosuje pliki cookies. Szczegóły w naszej Polityce prywatności. Kliknij \"Zgadzam się\", aby ta informacja nie wyświetlała się więcej.";
$polishTranslation["I agree"] = "Zgadzam się";
$polishTranslation["web pages, dedicated applications, mobile applications"] = "strony internetowe, aplikacje dedykowane, aplikacje mobilne";

return $polishTranslation;