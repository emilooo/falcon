<?php
$englishTranslation = array();

// CONTROLLERS

// Translation for controller of error
$englishTranslation['MessageForAccessDeniedContentHeader'] = 'Access denied';
$englishTranslation['MessageForAccessDeniedContentMessage'] = 'You haven\'t any privileges to this action.';
$englishTranslation['MessageForPageNotFoundContentHeader'] = 'Page not found';
$englishTranslation['MessageForPageNotFoundContentMessage'] = 'Sorry, but this page not exist.';
$englishTranslation['MessageForApplicationErrorContentHeader'] = 'Application error';
$englishTranslation['MessageForApplicationErrorContentMessage'] = 'Sorry, but this request has application error.';
$englishTranslation['MessageForBuldingPageContentHeader'] = "Service under construction";
$englishTranslation['MessageForBuldingPageContentMessage'] = 'Sorry, but service is not ready at the moment please come to here later.';

return $englishTranslation;