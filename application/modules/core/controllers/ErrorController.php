<?php
/**
 * @category Core
 * @package Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for primary controller of application
 * 
 * @category Core
 * @package Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Core_ErrorController
extends Infrastructure_Controller_Http_Error_Abstract
{
    public function init()
    {
        parent::init();
        
        $this->_registerExceptionHandler(
            "Zend_Controller_Dispatcher_Exception", "PageNotFound"
        );
        $this->_registerExceptionHandler(
            "Infrastructure_Acl_AccessDenied", "AccessDenied"
        );
        $this->_registerExceptionHandler(
            "Infrastructure_Exception_UnderConstruction", "UnderConstruction"
        );
    }
    
    public function setPageBreadcrumbs()
    {
    }
    
    public function setPageLinks(\Zend_View_Helper_HeadLink $headLinkHelper)
    {
        $headLinkHelper->setStylesheet(
            '/css/templates/error/screen.css'
        );
    }
    
    public function setPageMetaTags(\Zend_View_Helper_HeadMeta $headMetaHelper)
    {
    }
    
    public function setPageScripts(
        \Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
        $headScriptHelper->setAllowArbitraryAttributes(true);
        $headScriptHelper->appendFile(
            '/js/library/requirejs/require.js',
            'text/javascript',
            array('data-main'=>'/js/application/setup')
        );
    }
    
    public function setPageStyles(
        \Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function setPageTemplate(
        \Zend_View_Helper_Layout $layoutHelper
    )
    {
        $layoutHelper->layout()->setLayout('error');
    }
    
    public function setPageTitle(
        \Zend_View_Helper_HeadTitle $headTitleHelper
    )
    {
    }
}