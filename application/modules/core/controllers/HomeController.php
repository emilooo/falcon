<?php
/**
 * @category Core
 * @package Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for primary controller of application
 * 
 * @category Core
 * @package Controller
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Core_HomeController
extends Infrastructure_Controller_Http_Abstract
{
    public function init()
    {
        parent::init();
    }
    
    public function setPageBreadcrumbs()
    {
    }
    
    public function setPageLinks(\Zend_View_Helper_HeadLink $headLinkHelper)
    {
    }
    
    public function setPageMetaTags(\Zend_View_Helper_HeadMeta $headMetaHelper)
    {
    }
    
    public function setPageScripts(
        \Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
        $headScriptHelper->setAllowArbitraryAttributes(true);
        $headScriptHelper->appendFile(
            '/js/library/requirejs/require.js',
            'text/javascript',
            array('data-main'=>'/js/application/setup')
        );
    }
    
    public function setPageStyles(
        \Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function setPageTemplate(
        \Zend_View_Helper_Layout $layoutHelper
    )
    {
    }
    
    public function setPageTitle(
        \Zend_View_Helper_HeadTitle $headTitleHelper
    )
    {
    }
    
    public function indexAction()
    {
        throw new Infrastructure_Exception_UnderConstruction(
            "Current action is not ready at the moment"
        );
    }
}