<?php
/**
 * This source file is part of content management system
 *
 * @category Core
 * @package Core_Action
 * @subpackage ErrorController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Implements the interface to handling exceptions
 * 
 * @category Core
 * @package Core_Action
 * @subpackage ErrorController
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Core_Action_ErrorController_ApplicationError
extends Infrastructure_Action_Http_Error_Abstract
{
    public function makeAction()
    {
        return true;
    }
    
    public function appendToPageBreadcrumbs()
    {
    }
    
    public function appendToPageLinks(
        \Zend_View_Helper_HeadLink $headLinkHelper
    )
    {
    }
    
    public function appendToPageMetaTags(
        \Zend_View_Helper_HeadMeta $headMetaHelper
    )
    {
    }
    
    public function appendToPageScripts(
        \Zend_View_Helper_HeadScript $headScriptHelper
    )
    {
    }
    
    public function appendToPageStyles(
        \Zend_View_Helper_HeadStyle $headStyleHelper
    )
    {
    }
    
    public function appendToPageTitle(
        \Zend_View_Helper_HeadTitle $headTitleHelper
    )
    {
        $translatedHeadTitle
            = $headTitleHelper->getTranslator()
            ->translate("MessageForApplicationErrorContentHeader");
        $headTitleHelper->headTitle($translatedHeadTitle);
    }
    
    public function appendToTemplate(\Zend_View_Helper_Layout $layoutHelper)
    {
    }
    
    protected function _getContentHeader()
    {
        return 'MessageForApplicationErrorContentHeader';
    }
    
    protected function _getContentMessage()
    {
        return "MessageForApplicationErrorContentMessage";
    }
    
    protected function _getViewRendererName()
    {
    }
}