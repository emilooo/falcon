<form enctype="{$this->element->getEnctype()}" action="{$this->url()}" method="{$this->element->getMethod()}" name='{trim($this->element->getName())}' id="{trim($this->element->getName())}">
    {foreach from=$this->element->getElements() item="formFieldValue" key="formFieldKey"}
    <dl class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <dt class="col-xs-12 col-sm-3 col-md-3 col-lg-2"><label>{$this->element->{$formFieldValue->getName()}->getLabel()} </label></dt>
        <dd class="col-xs-12 col-sm-9 col-md-9 col-lg-10">
            <ul>
                <li>
                    {trim($this->element->{$formFieldValue->getName()}->renderViewHelper())}
                    {trim($this->element->{$formFieldValue->getName()}->renderErrors())}
                </li>
                
                {if !empty($formFieldValue->getAttrib('helpMessage'))}
                    <li><a href="#" title="{$this->translate($formFieldValue->getAttrib('helpMessage'))}" id="formInfoIcon">{$this->translate($formFieldValue->getAttrib('helpMessage'))}</a></li>
                {/if}
            </ul>
        </dd>
    </dl>
    {/foreach}
</form>