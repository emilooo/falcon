<header class="row">
    <h1>
        {if (isset($this->content["header"]))}
            {$this->translate($this->content["header"])}
        {/if}
    </h1>
</header>

<main class='row'>
    <p>
        {if (isset($this->content["message"]))}
            {$this->translate($this->content["message"])}
        {/if}
    </p>
    
    {assign var="isException" value=isset($this->error)}
    {assign var="isBuild" value=(APPLICATION_ENV == 'development'
        || APPLICATION_ENV == 'testing')}
    
    {if ($isException && $isBuild)}
        <section class='col-xs-12 col-md-12 col-lg-12' id='errors'>
            <header>
                <h3>{$this->translate("Exception")}</h3>
            </header>
            
            <dl>
                <dt class="col-xs-2 col-md-2 col-lg-1">
                    {$this->translate("Class")}:
                </dt>
                
                <dd class="col-xs-10 col-md-10 col-lg-11">
                    <code>{get_class($this->error)}</code>
                </dd>
                
                <dt class="col-xs-2 col-md-2 col-lg-1">
                    {$this->translate("Message")}:
                </dt>
                
                <dd class="col-xs-10 col-md-10 col-lg-11">
                    <code>{$this->error->getMessage()}</code>
                </dd>
                
                <dt class="col-xs-2 col-md-2 col-lg-1">
                    {$this->translate("File")}:
                </dt>
                
                <dd class="col-xs-10 col-md-10 col-lg-11">
                    <code>{$this->error->getFile()}</code>
                </dd>
                
                <dt class="col-xs-2 col-md-2 col-lg-1">
                    {$this->translate("Line")}:
                </dt>
                
                <dd class="col-xs-10 col-md-10 col-lg-11">
                    <code>{$this->error->getLine()}</code>
                </dd>
            </dl>
        </section>
    {/if}
</main>