<?php
/**
 * This source file is the part of emilooo.pl project
 *
 * @category Contact
 * @package Contact_Model
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for content of email message
 * 
 * @category Contact
 * @package Contact_Model
 * @subpackage Form
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Form_Email_Message
extends Infrastructure_Form_Abstract
{
    public function init()
    {
        parent::init();
        
        $this->addElement($this->_createSenderField());
        $this->addElement($this->_createSubjectField());
        $this->addElement($this->_createContentField());
        $this->addElement($this->_createSendButton());
    }
    
    /**
     * Returns the instance of sender form field
     * 
     * @return Zend_Form_Element_Text
     */
    private function _createSenderField()
    {
        $senderField = $this->createElement('text', 'sender');
        $senderField->setLabel('Email: ');
        $senderField->addFilter('StringTrim');
        $senderField->addValidator('NotEmpty', true);
        $senderField
            ->addValidator('StringLength', true, array('min'=>4, 'max'=> 50));
        $senderField->addValidator('EmailAddress', true);
        $senderField->setRequired(true);
        
        return $senderField;
    }
    
    /**
     * Returns the instance of subject form field
     * 
     * @return Zend_Form_Element_Text
     */
    private function _createSubjectField()
    {
        $subjectField = $this->createElement('text', 'subject');
        $subjectField->setLabel('Subject: ');
        $subjectField->addFilter('StringTrim');
        $subjectField->addFilter('Alnum');
        $subjectField->addValidator('NotEmpty', true);
        $subjectField
            ->addValidator('Alnum', true, array('allowWhiteSpace' => true));
        $subjectField
            ->addValidator('StringLength', true, array('min'=>4, 'max'=> 50));
        $subjectField->setRequired(true);
        
        return $subjectField;
    }
    
    /**
     * Returns the instance of content form field
     * 
     * @return Zend_Form_Element_Textarea
     */
    private function _createContentField()
    {
        $messageField = $this->createElement('textarea', 'content');
        $messageField->setLabel('Message: ');
        $messageField->addFilter('StringTrim');
        $messageField->addFilter('Alnum');
        $messageField->addValidator('NotEmpty', true);
        $messageField
            ->addValidator('Alnum', true, array('allowWhiteSpace' => true));
        $messageField
            ->addValidator('StringLength', true, array('min'=>4, 'max'=> 250));
        
        return $messageField;
    }
    
    /**
     * Returns the instance of send form button
     * 
     * @return Zend_Form_Element_Button
     */
    private function _createSendButton()
    {
        $sendButton = $this->createElement('button', 'Send');
        
        return $sendButton;
    }
}