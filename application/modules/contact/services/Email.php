<?php
/**
 * This source file is part of content management system
 *
 * @category Contact
 * @package Contact_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface for sending the e-mail message with the level
 * other module.
 * 
 * @category Contact
 * @package Contact_Service
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Service_Email
extends Infrastructure_Service_Abstract
implements Infrastructure_Service_Email_Interface
{
    /**
     * Stores the instance of email model
     * 
     * @var Infrastructure_Model_Email
     */
    protected $_model;
    
    /**
     * Stores the instance of content of e-mail message
     * 
     * @var Infrastructure_Form_Abstract
     */
    protected $_form;
    
    public function __construct($options = array())
    {
        parent::__construct($options);
    }
    
    public function init()
    {
        $this->_model = (empty($this->_model))
            ? new Contact_Model_Email() : $this->_model;
        $this->_form = (empty($this->_form))
            ? $this->_model->getForm('Message') : $this->_form;
    }
    
    public function sendMessage($sender, $subject, $content)
    {
        $dataToSend = array(
            'sender' => $sender,
            'subject' => $subject,
            'content' => $content
        );
        
        return $this->_model->sendMessage($dataToSend);
    }
    
    /**
     * Returns the errors if message has not sended
     * 
     * @return array
     */
    public function getErrors()
    {
        return $this->_form->getErrors();
    }
}