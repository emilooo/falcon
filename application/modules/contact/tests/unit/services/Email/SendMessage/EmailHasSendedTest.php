<?php
/**
 * @category Contact
 * @package Contact_Test_Unit_Service_Email
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Service/Email/SendMessage/'
    . 'EmailHasSendedTestCase.php';

/**
 * Testing the behavior of sendMessage method when input parameters
 * are correct and e-mail message has correctly sended
 * 
 * @category Contact
 * @package Contact_Test_Unit_Service_Email
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Test_Unit_Service_Email_SendMessage_EmailHasSendedTest
extends Infrastructure_Test_Unit_Service_Email_SendMessage_EmailHasSendedTestCase
{
    protected function _getFullServiceClassName()
    {
        return 'Contact_Service_Email';
    }
    
    protected function _getSender()
    {
        return 'test@example.com';
    }
    
    protected function _getSubject()
    {
        return 'Test message';
    }
    
    protected function _getContent()
    {
        return 'Test content message';
    }
}