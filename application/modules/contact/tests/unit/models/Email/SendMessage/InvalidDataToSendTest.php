<?php
/**
 * This source file is part of content management system
 *
 * @category Contact
 * @package Contact_Test_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Email/SendMessage/'
    . 'InvalidDataToSendTestCase.php';

/**
 * Testing the behavior of sendMessage method when input
 * parameter of dataToSend is invalid
 * 
 * @category Contact
 * @package Contact_Test_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Test_Unit_Model_Email_InvalidDataToSendTest
extends Infrastructure_Test_Unit_Model_Email_SendMessage_InvalidDataToSendTestCase
{
    protected function _getInvalidDataToSend()
    {
        return 'badValue';
    }
    
    protected function _getModelFullClassName()
    {
        return 'Contact_Model_Email';
    }
    
    protected function _getModelShortClassName()
    {
        return 'Email';
    }
    
    protected function _getFormName()
    {
        return 'Message';
    }
}