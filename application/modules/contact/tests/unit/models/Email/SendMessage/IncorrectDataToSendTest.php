<?php
/**
 * This source file is part of content management system
 *
 * @category Contact
 * @package Contact_Test_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of functional test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Email/SendMessage/'
    . 'IncorrectDataToSendTestCase.php';

/**
 * Testing the behavior of sendMessage method when input
 * parameter of dataToSend is incorrect
 * 
 * @category Contact
 * @package Contact_Test_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Test_Unit_Model_Email_IncorrectDataToSendTest
extends Infrastructure_Test_Unit_Model_Email_SendMessage_IncorrectDataToSendTestCase
{
    protected function _getIncorrectDataToSend()
    {
        return array(
            'sender' => 'test',
            'subject' => 'Test message from system',
            'content' => 'Hello world!'
        );
    }
    
    protected function _getFormName() {
        return 'Message';
    }
    
    protected function _getModelFullClassName()
    {
        return 'Contact_Model_Email';
    }
    
    protected function _getModelShortClassName()
    {
        return 'Email';
    }
}