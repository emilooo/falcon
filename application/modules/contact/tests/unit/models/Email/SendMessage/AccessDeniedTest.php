<?php
/**
 * This source file is part of content management system
 *
 * @category Contact
 * @package Contact_Test_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of unit test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Unit/Model/Email/SendMessage/'
    . 'AccessDeniedTestCase.php';

/**
 * Testing the behavior of sendMessage method when access to the method
 * is denied
 * 
 * @category Contact
 * @package Contact_Test_Unit_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Test_Unit_Model_Email_AccessDeniedTest
extends Infrastructure_Test_Unit_Model_Email_SendMessage_AccessDeniedTestCase
{
    protected function _getCorrectDataToSend()
    {
        return array(
            'sender' => 'test@example.com',
            'subject' => 'Test message from system',
            'content' => 'Hello world!'
        );
    }
    
    protected function _getFormName() {
        return 'Message';
    }
    
    protected function _getModelFullClassName()
    {
        return 'Contact_Model_Email';
    }
    
    protected function _getModelShortClassName()
    {
        return 'Email';
    }
}