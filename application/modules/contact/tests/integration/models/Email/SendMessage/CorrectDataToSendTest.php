<?php
/**
 * This source file is part of content management system
 *
 * @category Contact
 * @package Contact_Test_Integration_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of integration test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Email/SendMessage/'
    . 'CorrectDataToSendTestCase.php';

/**
 * Testing the behavior of sendMessage method when input
 * parameter of dataToSend is correct
 * 
 * @category Contact
 * @package Contact_Test_Integration_Model
 * @subpackage Email
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Test_Integration_Model_Email_CorrectDataToSendTest
extends Infrastructure_Test_Integration_Model_Email_SendMessage_CorrectDataToSendTestCase
{
    protected function _getCorrectDataToSend()
    {
        return array(
            'sender' => 'admin@example.com',
            'subject' => 'Test message from system',
            'content' => 'Hello world!'
        );
    }
    
    protected function _getFullResourceClassName()
    {
        return 'Contact_Model_Resource_Email';
    }
    
    protected function _getFormName() {
        return 'Message';
    }
    
    protected function _getModelFullClassName()
    {
        return 'Contact_Model_Email';
    }
    
    protected function _getModelShortClassName()
    {
        return 'Email';
    }
    
    protected function _getEmailTestDirectory()
    {
        $configuration = $this->getConfiguration();
        
        return $configuration['email']['testDirectory'];
    }
}

