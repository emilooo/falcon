<?php
/**
 * This source file is part of content management system
 *
 * @category Contact
 * @package Contact_Test_Integration_Model_Email
 * @subpackage SendMessage
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Path to interface of integration test case
 */
require_once ROOT_PATH
. 'library/Infrastructure/Test/Integration/Model/Email/SendMessage/'
    . 'IncorrectDataToSendTestCase.php';

/**
 * Testing the behavior of sendMessage method when input
 * parameter of dataToSend is incorrect
 * 
 * @category Contact
 * @package Contact_Test_Integration_Model_Email
 * @subpackage SendMessage
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Test_Integration_Model_Email_IncorrectDataToSendTest
extends Infrastructure_Test_Integration_Model_Email_SendMessage_IncorrectDataToSendTestCase
{
    protected function _getIncorrectDataToSend()
    {
        return array(
            'sender' => 'test',
            'subject' => 'Test message from system',
            'content' => 'Hello world!'
        );
    }
    
    protected function _getFormName() {
        return 'Message';
    }
    
    protected function _getModelFullClassName()
    {
        return 'Contact_Model_Email';
    }
    
    protected function _getModelShortClassName()
    {
        return 'Email';
    }
}
