<?php
/**
 * This source file is the part of emilooo.pl project
 *
 * @category Contact
 * @package Contact_Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Provides the interface to sending email messages
 * 
 * @category Contact
 * @package Contact_Model
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Model_Email
extends Infrastructure_Model_Email
{
    public function init()
    {
    }
    
    public function setAcl(\Infrastructure_Acl_Interface $acl)
    {
        $getResourceIdResult = $this->getResourceId();
        if (!$acl->has($getResourceIdResult)) {
            $acl->add($this)
                ->allow('Guest', $this, array('sendMessage'))
                ->deny(
                    'Guest', $this, array()
                )->allow(
                    'User',
                    $this,
                    array(
                        'sendMessage',
                    )
                )->allow('Admin', $this);
        }
        $this->_acl = $acl;
        return $this;
    }
    
    public function getAcl()
    {
        if (empty($this->_acl)) {
            $structureAcl = new Structure_Model_Acl();
            $this->setAcl($structureAcl);
        }
        
        return $this->_acl;
    }
    
    public function getResourceId()
    {
        return 'Email';
    }
    
    protected function _getForm()
    {
        return $this->getForm('Message');
    }
}