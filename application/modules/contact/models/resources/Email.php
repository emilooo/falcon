<?php
/**
 * This source file is the part of emilooo.pl project
 *
 * @category Contact
 * @package Contact_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Returns the special name for email message which is storing in the file
 * 
 * @return string
 */
function getEmailFieleName()
{
    return "testEmail_" . mt_rand() . '.txt';
}

/**
 * Implements the interface to handling the operations of error controller
 * 
 * @category Contact
 * @package Contact_Model
 * @subpackage Resource
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Contact_Model_Resource_Email
extends Infrastructure_Model_Resource_Email
{
    protected function _makeTransprt()
    {
        $getConfiguration = $this->_configuration;
        
        if (APPLICATION_ENV != "testing") {
            $transport = new Zend_Mail_Transport_Smtp(
                $getConfiguration['hostname'], $getConfiguration['params']
            );
        } else {
            $transport = new Zend_Mail_Transport_File(
                array(
                    "path" => $getConfiguration["testDirectory"],
                    "callback" => "getEmailFieleName"
                )
            );
        }
        
        return $transport;
    }
    
    protected function _makeEmail($dataToSend)
    {
        $getConfiguration = $this->_configuration;
        
        $mail = new Zend_Mail('UTF-8');
        $mail->setFrom($dataToSend['sender']);
        $mail->addTo($getConfiguration["params"]["username"]);
        $mail->setSubject($dataToSend['subject']);
        $mail->setBodyText($dataToSend['content']);
        
        return $mail;
    }
    
    protected function _getConfiguration()
    {
        $getConfiguration = Zend_Registry::get('config');
        $getEmailConfiguration = $getConfiguration['email'];
        
        return $getEmailConfiguration;
    }
}