<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="{$this->translate()->getLocale()}" class="no-js"> <!--<![endif]-->
    <head>
        {$this->headTitle()}
        
        {$this->headMeta()}
        
        {$this->headStyle()}
        
        {$this->headLink()}
    </head>
    <body>
        <section class="page-content container">
            {$this->layout()->content}
        </section>
        
        <footer class="page-footer">
            <div class='copright'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                            <p>&copy; {$this->translate('Copright by')} <a href="/{$this->translate()->getLocale()}/">Author</a>. {$this->translate('All right reserved')}</p>
                        </div>
                        <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                            <p class="text-right">{$this->translate('Powered by')}: <a href="/{$this->translate()->getLocale()}/">Software name</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        {$this->headScript()}
    </body>
</html>