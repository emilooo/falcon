<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--><html lang="{$this->translate()->getLocale()}" class="no-js"> <!--<![endif]-->
    <head>
        {$this->headTitle()}
        
        {$this->headMeta()}
        
        {$this->headStyle()}
        
        {$this->headLink()}
    </head>
    <body>
        <!--[if lt IE 7]>
            <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
        <![endif]-->
        
        <div class="container">
            <div class="row">
                <div class="hidden-xs hidden-sm col-md-2 col-lg-2">
                    <div id="brand">
                        <img src="/img/application/templates/admin/short-logo.png" title="Logotype" />
                    </div>
                    
                    <section id="userStatus">
                        <p><span id="userIcon"></span>{$this->translate('Logged in us')}: <strong>{$this->userLogin}</strong></p>
                        
                        <ul>
                            <li><a href="{$this->url(array(), 'profileUser', true)}/{$this->userLogin}" title="{$this->translate('Show the user profile')}" class="showProfile">{$this->translate('Profile')}</a></li>
                            <li><a href="{$this->url(array(), 'logoutUser', true)}" title="{$this->translate('Log out user')}" class="logoutUser">{$this->translate('Log out')}</a></li>
                        </ul>
                    </section>
                    
                    {include file='./admin/_menu.tpl'}
                </div>
                
                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-10">
                    <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        <ul>
                            {$polishLanguage['lang']="pl"}
                            {$englishLanguage['lang']="en"}
                            {if ($this->translate()->getLocale() == 'pl')}
                                <li><a href="{$this->url($polishLanguage)}" title="Change to polish language" id="polishLanguageIcon" class="active">Change to polish language</a></li>
                                <li><a href="{$this->url($englishLanguage)}" title="Change to english language" id="englishLanguageIcon">Change to english language</a></li>
                            {else}
                                <li><a href="{$this->url($polishLanguage)}" title="Change to polish language" id="polishLanguageIcon">Change to polish language</a></li>
                                <li><a href="{$this->url($englishLanguage)}" title="Change to english language" id="englishLanguageIcon" class="active">Change to english language</a></li>
                            {/if}
                        </ul>
                    </div>
                    
                    <section class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                        {$this->layout()->content}
                    </section>
                </div>
            </div>
            
            <footer class="page-footer row">
                <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                    <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5 col-md-offset-2 col-lg-offset-2">
                        <p>&copy; {$this->translate('Copright by')} <a href="/{$this->translate()->getLocale()}/">emilooo</a>. {$this->translate('All right reserved')}</p>
                    </div>

                    <div class="col-xs-6 col-sm-6 col-md-5 col-lg-5">
                        <p class="text-right">{$this->translate('Powered by')}: <a href="/{$this->translate()->getLocale()}/">Emilooo CMS</a></p>
                    </div>
                </div>
            </footer>
        </div>
        
        {$this->headScript()}
    </body>
</html>