<!DOCTYPE html>
<!--[if lt IE 7]><html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]><html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]><html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!-->
<html lang="{$this->translate()->getLocale()}" class="no-js"> <!--<![endif]-->
    <head>
        {$this->headTitle()}
        
        {$this->headMeta()}
        
        {$this->headStyle()}
        
        {$this->headLink()}
        
        {$this->headScript()}
    </head>
    <body>
        <div id="wrap">
            <div id="main" class="container">
                <div class="row">
                    <section class="col-md-2 col-centered page-content">
                        {$this->layout()->content}
                    </section>
                </div>
            </div>
        </div>
        
        <footer class="page-footer">
            <div class='copright'>
                <div class='container'>
                    <div class='row'>
                        <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                            <p>&copy; {$this->translate('Copright by')} <a href="/{$this->translate()->getLocale()}/">emilooo</a>. {$this->translate('All right reserved')}</p>
                        </div>
                        <div class='col-xs-6 col-sm-6 col-md-6 col-lg-6'>
                            <p class="text-right">{$this->translate('Powered by')}: <a href="/{$this->translate()->getLocale()}/">Emilooo CMS</a></p>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
    </body>
</html>