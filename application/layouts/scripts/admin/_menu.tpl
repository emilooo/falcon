<nav>
    <ul>
        {if (strstr($this->url(), 'users'))}
            <li class='active'><a href="{$this->url(array(), 'listUsers')}" title="{$this->translate('Moderate users')}">{$this->translate('Users')}</a></li>
        {else if}
            <li><a href="{$this->url(array(), 'listUsers')}" title="{$this->translate('Moderate users')}">{$this->translate('Users')}</a></li>
        {/if}
    </ul>
</nav>