<html lang='{$this->translate()->getLocale()}'>
    <head>
        {$this->headTitle()}
        
        {$this->headMeta()}
        
        {$this->headStyle()}
        
        {$this->headLink()}
    </head>
    
    <body>
        {$this->layout()->content}
        {$this->headScript()}
    </body>
</html>