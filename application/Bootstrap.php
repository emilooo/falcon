<?php
/**
 * @category Application
 * @package Application
 * @subpackage Bootstrap
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Loads the default configuration of namespace of application
 */
require_once APPLICATION_PATH . '/bootstrap/InitiateApplicationModule.php';

/**
 * Loads the default configuration of namespace of application
 */
require_once APPLICATION_PATH . '/bootstrap/InitiateViewSettings.php';

/**
 * Implements the interface to handling the operations of application bootstrap
 * 
 * @category Application
 * @package Application
 * @subpackage Bootstrap
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Application_Bootstrap
extends Infrastructure_Bootstrap_Abstract
{
    protected function _initLogging()
    {
        $initLog = new Structure_Bootstrap_Element_InitZendLog($this);
        $initLog->initiate();
        $this->_logger = Zend_Registry::get('log');
    }
    
    /**
     * Adds the application configuration to registry
     * 
     * @return void
     */
    protected function _initConfig()
    {
        $this->_logger->info('Bootstrap ' . __METHOD__);
        Zend_Registry::set('config', $this->getOptions());
    }
    
    protected function _initTranslation()
    {
        $this->_logger->info('Bootstrap ' . __METHOD__);
        $translate = new Zend_Translate(
            'Array',
            ROOT_PATH . '/application/modules/core/language/english.php',
            'en'
        );
        $translate->addTranslation(
            ROOT_PATH . '/application/modules/core/language/polish.php',
            'pl'
        );
        $translate->addTranslation(
            ROOT_PATH . '/application/modules/authorization/language/english.php',
            'en'
        );
        $translate->addTranslation(
            ROOT_PATH . '/application/modules/authorization/language/polish.php',
            'pl'
        );
        $translate->setLocale('pl');
        Zend_Registry::set('Zend_Translate', $translate);
    }
    
    /**
     * Initiates the application view settings
     * 
     * @return void
     */
    protected function _initViewSettings()
    {
        $this->_logger->info('Bootstrap ' . __METHOD__);
        $viewSetting = $this->getElement('InitiateViewSettings');
        $viewSetting->initiate();
    }
    
    /**
     * Initiates the application smarty view renderer
     * 
     * @return void
     */
    protected function _initViewSmarty()
    {
        $this->_logger->info('Bootstrap ' . __METHOD__);
        $initViewSmarty = new Structure_Bootstrap_Element_InitSmarty($this);
        $initViewSmarty->initiate();
    }
    
    /**
     * Adds the controller action helpers
     * 
     * @return void
     */
    protected function _initControllerHelpers()
    {
        $this->_logger->info('Bootstrap ' . __METHOD__);
        Zend_Controller_Action_HelperBroker::addHelper(
            new Structure_Helper_Controller_Acl()
        );
    }
    
    protected function _initPlugins()
    {
        $front = Zend_Controller_Front::getInstance();
        
        // register the plugin
        $front->registerPlugin(new Structure_Plugin_ErrorSwicher());
        $front->registerPlugin(new Structure_Plugin_LanguageSwicher());
    }
    
    /**
     * Initiates the database profiler
     * 
     * @return void
     */
    protected function _initDatabaseProfiler()
    {
        $this->_logger->info('Bootstrap ' . __METHOD__);
        
        if ('production' !== $this->getEnvironment()) {
            $this->bootstrap('db');
            $profiler = new Zend_Db_Profiler_Firebug('All DB Queries');
            $profiler->setEnabled(true);
            $this->getPluginResource('db')->getDbAdapter()
                ->setProfiler($profiler);
        }
    }
    
    /**
     * Initiates the database table cache
     * 
     * @return void
     */
    protected function _initDatabaseTableCache()
    {
        $this->_logger->info('Bootstrap ' . __METHOD__);
        if ('production' == $this->getEnvironment()) {
            $getConfigResults = Zend_Registry::get('config');
            $backendOptions = $getConfigResults['resources']['cachemanager']
                ['database']['table'];
            $cache = Zend_Cache::factory('Core', 'Memcached', $backendOptions);
            Zend_Db_Table_Abstract::setDefaultMetadataCache($cache);
        }
    }
    
    /**
     * Initiates the module of application
     */
    protected function _initApplicationModule()
    {
        $this->getElement('InitiateApplicationModule')->initiate();
    }
    
    /**
     * Initiates the module of core
     * 
     * @return void
     */
    protected function _initCoreModule()
    {
        $initiateResult = $this->getElement('InitiateCoreModule')->initiate();
    }
    
    /**
     * Initiates the routes for module of core
     * 
     * @return void
     */
    protected function _initCoreRoutes()
    {
        $this->getElement('InitiateCoreRoutes')->initiate();
    }
    
    /**
     * Initiate the module of authorization
     * 
     * @return void
     */
    protected function _initAuthorizationModule()
    {
        $initiateAuthorizationModuleResult
            = $this->getElement("InitiateAuthorizationModule")->initiate();
    }
    
    protected function _initAuthorizationRoutes()
    {
        $this->getElement('InitiateAuthorizationRoutes')->initiate();
    }
    
    protected function _initContactModule()
    {
        $this->getElement('InitiateContactModule')->initiate();
    }
    
    /**
     * Initiate the module of rest
     * 
     * @return void
     */
    protected function _initRestModule()
    {
        $this->getElement('InitiateRestModule')->initiate();
    }
}

