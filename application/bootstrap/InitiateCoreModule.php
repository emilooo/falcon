<?php
/**
 * This source file is part of content management system
 *
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the configuration for namespace of core
 * 
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Application_Bootstrap_Element_InitiateCoreModule
extends Infrastructure_Bootstrap_Element_Abstract
{
    public function initiate()
    {
        $autoloader = new Zend_Application_Module_Autoloader(
            array(
                'namespace' => 'Core',
                'basePath' => APPLICATION_PATH . '/modules/core',
                'resourceTypes' => $this->_getResources()
            )
        );
        
        return $autoloader;
    }
    
    /**
     * Returns the config for resources to the autoloader
     * 
     * @return array
     */
    private function _getResources()
    {
        return array(
            'modelResource' => $this->_getModelResource(),
            'aclResource' => $this->_getAclResource(),
            'validatorResource' => $this->_getValidatorResource(),
            'actionResource' => $this->_getActionResource(),
        );
    }
    
    /**
     * Returns the config for model resource to the autoloader
     * 
     * @return array
     */
    private function _getModelResource()
    {
        return array(
            'path' => 'models/resources/',
            'namespace' => 'Model_Resource'
        );
    }
    
    /**
     * Returns the config for acl resource to the autoloader
     * 
     * @return array
     */
    private function _getAclResource()
    {
        return array(
            'path' => 'models/acls/',
            'namespace' => 'Model_Acl'
        );
    }
    
    /**
     * Returns the config for validator resource to the autoloader
     * 
     * @return array
     */
    private function _getValidatorResource()
    {
        return array(
            'path' => 'models/validates',
            'namespace' => 'Model_Validate'
        );
    }
    
    
    /**
     * Returns the config for action resource to the autoloader
     * 
     * @return array
     */
    protected function _getActionResource()
    {
        return array(
            'path' => 'actions',
            'namespace' => 'Action'
        );
    }
}