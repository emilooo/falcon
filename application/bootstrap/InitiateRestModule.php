<?php
/**
 * This source file is part of content management system
 *
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the configuration for namespace of rest
 * 
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Application_Bootstrap_Element_InitiateRestModule
extends Infrastructure_Bootstrap_Element_Abstract
{
    /**
     * Returns the instance of autoloader
     * 
     * @return \Zend_Application_Module_Autoloader
     */
    public function initiate()
    {
        $autoloader = new Zend_Application_Module_Autoloader(
            array(
                'namespace' => 'Rest',
                'basePath' => APPLICATION_PATH . '/modules/rest',
                'resourceTypes' => $this->_getResources()
            )
        );
        
        return $autoloader;
    }
    
    /**
     * Returns the config for resources
     * 
     * @return array
     */
    private function _getResources()
    {
        return array(
            'modelResource' => $this->_getModelResource(),
            'modelAcl' => $this->_getAclResource(),
            'modelFilter' => $this->_getFilterResource(),
            'actionResource' => $this->_getActionResource()
        );
    }
    
    /**
     * Rteurns the config for model resource
     * 
     * @return array
     */
    private function _getModelResource()
    {
        return array(
            'path' => 'models/resources/',
            'namespace' => 'Model_Resource'
        );
    }
    
    /**
     * Returns the config for acl resource
     * 
     * @return array
     */
    private function _getAclResource()
    {
        return array(
            'path' => 'models/acls/',
            'namespace' => 'Model_Acl'
        );
    }
    
    /**
     * Returns the config for filter resource
     * 
     * @return array
     */
    private function _getFilterResource()
    {
        return array(
            'path' => 'models/filters',
            'namespace' => 'Model_Filter'
        );
    }
    
    /**
     * Returns the config for action resource to the autoloader
     * 
     * @return array
     */
    protected function _getActionResource()
    {
        return array(
            'path' => 'actions',
            'namespace' => 'Action'
        );
    }
}