<?php
/**
 * This source file is part of content management system
 *
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the routes for module of user paths
 * 
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Application_Bootstrap_Element_InitiateAuthorizationRoutes
extends Infrastructure_Bootstrap_Element_Abstract
{
    public function initiate()
    {
        $getRouter = $this->getBootstrap()->frontController->getRouter();
        $getLoginRoute = $this->_makeLoginUserRoute();
        $getRouter->addRoute('loginUser', $getLoginRoute);
        $getLogoutRoute = $this->_makeLogoutUserRoute();
        $getRouter->addRoute('logoutUser', $getLogoutRoute);
        $getRegisterRoute = $this->_makeRegisterUserRoute();
        $getRouter->addRoute('registerUser', $getRegisterRoute);
        $getModerateRoute = $this->_makeListUsersRoute();
        $getRouter->addRoute('listUsers', $getModerateRoute);
        $getProfileRoute = $this->_makeProfileUserRoute();
        $getRouter->addRoute('profileUser', $getProfileRoute);
        $getCreateRoute = $this->_makeCreateUserRoute();
        $getRouter->addRoute('createUser', $getCreateRoute);
        $getDeleteRoute = $this->_makeDeleteUserRoute();
        $getRouter->addRoute('deleteUser', $getDeleteRoute);
        $getUpdateRoute = $this->_makeUpdateUserRoute();
        $getRouter->addRoute('updateUser', $getUpdateRoute);
    }
    
    /**
     * Makes the route for path of login
     * 
     * @return \Zend_Controller_Router_Route
     */
    private function _makeLoginUserRoute()
    {
        $loginRoute = new Zend_Controller_Router_Route(
            '/:lang/login',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'login',
                'lang' => 'en'
            )
        );
        
        return $loginRoute;
    }
    
    /**
     * Makes the route for path of logout
     * 
     * @return \Zend_Controller_Router_Route
     */
    private function _makeLogoutUserRoute()
    {
        $logoutRoute = new Zend_Controller_Router_Route(
            '/:lang/logout',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'logout',
                'lang' => 'en'
            )
        );
        
        return $logoutRoute;
    }
    
    /**
     * Makes the route for path of register
     * 
     * @return \Zend_Controller_Router_Route
     */
    private function _makeRegisterUserRoute()
    {
        $logoutRoute = new Zend_Controller_Router_Route(
            '/:lang/register',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'register',
                'lang' => 'en'
            )
        );
        
        return $logoutRoute;
    }
    
    private function _makeListUsersRoute()
    {
        $moderateRoute = new Zend_Controller_Router_Route(
            '/:lang/users/moderate/:page',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'index',
                'page' => 1,
                'lang' => 'en'
            )
        );
        
        return $moderateRoute;
    }
    
    private function _makeCreateUserRoute()
    {
        $createRoute = new Zend_Controller_Router_Route(
            '/:lang/users/create',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'create',
                'lang' => 'en'
            )
        );
        
        return $createRoute;
    }
    
    private function _makeProfileUserRoute()
    {
        $profileRoute = new Zend_Controller_Router_Route(
            '/:lang/users/profile/:login',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'profile',
                'login' => null,
                'lang' => 'en'
            )
        );
        
        return $profileRoute;
    }
    
    private function _makeUpdateUserRoute()
    {
        $updateRoute = new Zend_Controller_Router_Route(
            '/:lang/users/update/:id',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'update',
                'id' => null,
                'lang' => 'en'
            )
        );
        
        return $updateRoute;
    }
    
    private function _makeDeleteUserRoute()
    {
        $removeRoute = new Zend_Controller_Router_Route(
            '/:lang/users/remove/:id',
            array(
                'module' => 'authorization',
                'controller' => 'user',
                'action' => 'delete',
                'id' => null,
                'lang' => 'en'
            )
        );
        
        return $removeRoute;
    }
}
