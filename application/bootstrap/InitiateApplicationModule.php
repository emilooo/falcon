<?php
/**
 * This source file is part of content management system
 *
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the configuration of namespace of application
 * 
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Application_Bootstrap_Element_InitiateApplicationModule
extends Infrastructure_Bootstrap_Element_Abstract
{
    public function initiate()
    {
        $autoloader = new Zend_Application_Module_Autoloader(
            array(
                'namespace' => 'Application',
                'basePath' => APPLICATION_PATH,
                'resourceTypes' => $this->_getResourceTypes()
            )
        );
        
        return $autoloader;
    }
    
    private function _getResourceTypes()
    {
        return array(
            'elementResources' => $this->_getElementResource(),
        );
    }
    
    private function _getElementResource()
    {
        return array(
            'path' => 'bootstrap/',
            'namespace' => 'Bootstrap_Element'
        );
    }
}

