<?php
/**
 * This source file is part of content management system
 *
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the routes for module of core paths
 * 
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Application_Bootstrap_Element_InitiateCoreRoutes
extends Infrastructure_Bootstrap_Element_Abstract
{
    public function initiate()
    {
        $getRouter = $this->getBootstrap()->frontController->getRouter();
        $homeRoute = $this->_makeHomeRoute();
        
        $getRouter->addRoute("homeRoute", $homeRoute);
    }
    
    /**
     * Makes the route for path of login
     * 
     * @return \Zend_Controller_Router_Route
     */
    private function _makeHomeRoute()
    {
        $homeRoute = new Zend_Controller_Router_Route(
            '/:lang/',
            array(
                'module' => 'core',
                'controller' => 'home',
                'action' => 'index',
                'lang' => 'en'
            )
        );
        
        return $homeRoute;
    }
}

