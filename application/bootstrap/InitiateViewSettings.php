<?php
/**
 * This source file is part of content management system
 *
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Initiates the view settings through:
 * - sets the default encoding
 * - sets the settings of meta
 * - sets the title of page
 * 
 * @category Application
 * @package Application_Bootstrap
 * @subpackage Element
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Application_Bootstrap_Element_InitiateViewSettings
extends Structure_Bootstrap_Element_InitViewSettings
{
    protected function _addEncoding()
    {
        $view = $this->_getView();
        $view->setEncoding('UTF-8');
    }
    
    protected function _addHeadMeta()
    {
        $getConfigurationResult = $this->_getConfiguration();
        $headMeta = $this->_getView()->headMeta();
        
        $headMeta->appendHttpEquiv('X-UA-Compatible', 'IE=edge,chrome=1');
        $headMeta->appendHttpEquiv('Content-Language', 'en-US');
        $headMeta->appendHttpEquiv("Content-type", "text/html;charset=utf-8");
        $headMeta->appendName('viewport', 'width=device-width, initial-scale=1');
        $headMeta->appendName('author', $this->_getProjectAuthor());
        $headMeta->appendName('descryption', $this->_getProjectDescription());
        $headMeta->appendName('keywords', $this->_getProjectKeywords());
    }
    
    protected function _addHeadTitle()
    {
        $headTitle = $this->_getView()->headTitle($this->_getProjectName());
        $headTitle->setSeparator(' / ');
    }
    
    /**
     * @return Zend_Config
     */
    private function _getConfiguration()
    {
        $getConfigurationResult = Zend_Registry::get("config");
        
        return $getConfigurationResult;
    }
    
    /**
     * @return Zend_Translate
     */
    private function _getTranslator()
    {
        $getTranslatorResult = Zend_Registry::get("Zend_Translate");
        
        return $getTranslatorResult;
    }
    
    /**
     * Returns the translated project name
     * 
     * @return string
     */
    private function _getProjectName()
    {
        $getConfigurationResult = $this->_getConfiguration();
        $getTranslationResult = $this->_getTranslator();
        
        return $getTranslationResult->translate(
            $getConfigurationResult["project"]["name"]
        );
    }
    
    /**
     * Returns the translated project author
     * 
     * @return string
     */
    private function _getProjectAuthor()
    {
        $getConfigurationResult = $this->_getConfiguration();
        $getTranslationResult = $this->_getTranslator();
        
        return $getTranslationResult->translate(
            $getConfigurationResult["project"]["author"]
        );
    }
    
    /**
     * Returns the translated project description
     * 
     * @return string
     */
    private function _getProjectDescription()
    {
        $getConfigurationResult = $this->_getConfiguration();
        $getTranslationResult = $this->_getTranslator();
        
        return $getTranslationResult->translate(
            $getConfigurationResult["project"]["description"]
        );
    }
    
    /**
     * Returns the translated project keywords
     * 
     * @return string
     */
    private function _getProjectKeywords()
    {
        $getConfigurationResult = $this->_getConfiguration();
        $getTranslationResult = $this->_getTranslator();
        
        return $getTranslationResult->translate(
            $getConfigurationResult["project"]["keywords"]
        );
    }
}