<?php
//define error reporting for tests
error_reporting(
    E_ALL | E_STRICT
);

//define path to primary directory of application
defined('BASE_PATH')
    || define('BASE_PATH', realpath(dirname(__FILE__) . '/../') . '/');

defined('ROOT_PATH')
    || define(
        'ROOT_PATH',
        BASE_PATH . '/'
    );

//define path to application directory
defined('APPLICATION_PATH')
    || define(
        'APPLICATION_PATH',
        BASE_PATH . 'application/'
    );

//define path to tests directory
defined('TESTS_PATH')
    || define(
        'TESTS_PATH',
        BASE_PATH . 'tests/'
    );

//define path to tests directory
defined('FIXTURE_PATH')
    || define(
        'FIXTURE_PATH',
        BASE_PATH . 'test/fixtures/'
    );

//define application environment state
defined('APPLICATION_ENV')
    || define(
        'APPLICATION_ENV',
        (getenv('APPLICATION_ENV') ? getenv('APPLICATION_ENV') : 'testing')
    );

defined('APPLICATION_CONFIGURATION_FILE')
    || define(
        'APPLICATION_CONFIGURATION_FILE',
        APPLICATION_PATH . 'configs/application.ini'
    );

set_include_path(
    implode(
        PATH_SEPARATOR,
        array(
            realpath(APPLICATION_PATH . '/../library'),
            get_include_path()
        )
    )
);

//Use autoload from resoult of composer
require_once '../vendor/autoload.php';

/**
 * Loads the test case which stores the information about database connection.
 */
require_once TESTS_PATH . 'application/DatabaseTestCase.php';

//Initiate session
Zend_Session::$_unitTestEnabled = true;
Zend_Session::start();

//Initiate the zend framework to use with phpunit
$zendApplicationInstance = new Zend_Application(
    APPLICATION_ENV,
    APPLICATION_CONFIGURATION_FILE
);
$zendApplicationInstance->bootstrap();