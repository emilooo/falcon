<?php
/**
 * This source file is part of content management system
 *
 * @category Test
 * @package Test_Application_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */

/**
 * Testing the behavior of method of isValid when input parameter
 * of role are empty
 * 
 * @category Test
 * @package Test_Application_Functional_Controller
 * @subpackage Error
 * @author Emil Maraszek <programmer@emilooo.pl>
 */
class Test_Application_Functional_ErrorController_InvalidControllerTest
extends Infrastructure_Test_Functional_Controller_TestCase
{
    public function test_runInvalidController_true()
    {
        $getModuleName = $this->_getModuleName();
        $getControllerName = $this->_getControllerName();
        $getAction = $this->_getActionName();
        
        $this->dispatch(
            "/" . $getModuleName . "/" . $getControllerName . "/"
            . $getAction . "/"
        );
        
        $this->assertModule("core");
        $this->assertController("error");
        $this->assertAction("error");
    }
    
    protected function _getModuleName()
    {
        return 'core';
    }
    
    protected function _getControllerName()
    {
        return 'pageNotFound';
    }
    
    protected function _getActionName()
    {
        return 'actionNotFound';
    }
}
