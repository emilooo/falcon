<?php
/**
 * @category Test
 * @package Test_Application_DatabaseTestCase
 */

/**
 * Provides the interface for testing database integration structure
 * 
 * @category Test
 * @package Test_Application_DatabaseTestCase
 */
abstract class Test_Application_DatabaseTestCase
extends Zend_Test_PHPUnit_DatabaseTestCase
{
    /**
     * @var Zend_Test_PHPUnit_Db_Connection 
     */
    private $_connection;
    
    /**
     * @return Zend_Test_PHPUnit_Db_Connection
     */
    public function getConnection()
    {
        if ($this->_connection === null) {
            $getConfiguration = $this->_getConfiguration();
            $getConnectionByConfiguration
                = $this->_getConnectionByConfiguration($getConfiguration);
            $this->_connection = $getConnectionByConfiguration;
        }
        
        return $this->_connection;
    }
    
    /**
     * @return Zend_Config_Ini
     */
    private function _getConfiguration()
    {
        $configuration = new Zend_Config_Ini(
            APPLICATION_CONFIGURATION_FILE,
            APPLICATION_ENV
        );
        
        return $configuration;
    }
    
    /**
     * @param Zend_Config_Ini $configuration
     * @return Zend_Test_PHPUnit_Db_Connection
     */
    private function _getConnectionByConfiguration(
        Zend_Config_Ini $configuration
    )
    {
        $connection = Zend_Db::factory(
            $configuration->resources->db
        );
        $createZendDbConnectionResult = $this->createZendDbConnection(
            $connection,
            $configuration->resources->db->params->dbname
        );
        Zend_Db_Table_Abstract::setDefaultAdapter($connection);
        
        return $createZendDbConnectionResult;
    }
}