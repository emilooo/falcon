-- Stores the user data
CREATE TABLE IF NOT EXISTS users (
    user_id INT(3) AUTO_INCREMENT NOT NULL,
    user_login VARCHAR(20) UNIQUE NOT NULL,
    user_firstname VARCHAR(15) NOT NULL,
    user_lastname VARCHAR(20) NOT NULL,
    user_email VARCHAR(50) UNIQUE NOT NULL,
    user_password VARCHAR(32) NOT NULL,
    user_salt VARCHAR(32) NOT NULL,
    user_role VARCHAR(20) NOT NULL,
    user_status VARCHAR(20) NOT NULL,
    INDEX (`user_login`, `user_email`, `user_role`, `user_status`),
    PRIMARY KEY(user_id)
) ENGINE = InnoDB DEFAULT CHARSET = utf8;

-- //@UNDO

DROP TABLE IF EXISTS `users`;