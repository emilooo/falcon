module.exports = function(grunt) {
    'use strict';
    grunt.initConfig({
        bower: {
            install: {
                options: {
                    targetDir: './js/library/',
                    layout: 'byType',
                    install: true,
                    verbose: false,
                    cleanTargetDir: true,
                    cleanBowerDir: false,
                    bowerOptions: {}
                }
            }
        },
        copy: {
            "bootstrap-scss-files": {
                expand: true,
                flatten: true,
                src: "bower_components/bootstrap-sass/assets/stylesheets/_bootstrap.scss",
                dest: "scss/library/bootstrap/",
                filter: "isFile",
                options: {
                    process: function(content, srcPath) {
                        var convertSassPathResult = content.replace(
                            /bootstrap/g,
                            "../../../bower_components/bootstrap-sass/assets/stylesheets/bootstrap"
                        );
                
                        return convertSassPathResult;
                    }
                }
            },
            "bootstrap-font-files": {
                expand: true,
                flatten: true,
                src: "bower_components/bootstrap-sass/assets/fonts/bootstrap/*",
                dest: "fonts/library/bootstrap/",
                filter: "isFile"
            },
            "jqueryui-css-files": {
                expand: true,
                flatten: true,
                src: "bower_components/jquery-ui/themes/base/*.css",
                dest: "css/library/jquery-ui/themes/base/",
                filter: "isFile",
                options: {
                    process: function(content, srcPath) {
                        var convertSassPathResult = content.replace(
                            /images/g,
                            "/img/library/jquery-ui/themes/base"
                        );
                
                        return convertSassPathResult;
                    }
                }
            },
            "jqueryui-img-files": {
                expand: true,
                flatten: true,
                src: "bower_components/jquery-ui/themes/base/images/*",
                dest: "img/library/jquery-ui/themes/base/",
                filter: "isFile"
            }
        },
        rename: {
            "bootstrap-sass": {
                src: "scss/library/bootstrap/_bootstrap.scss",
                dest: "scss/library/bootstrap/bootstrap.scss"
            }
        }
    });
    
    grunt.loadNpmTasks('grunt-bower-task');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-rename');
    grunt.registerTask('default', ['bower', 'copy', 'rename']);
};
