define([
    'namespace',
    'jquery',
    'underscore',
    'backbone'
], function(Namespace, $, _, Backbone) {
    Namespace.Application.Events.Error || (
        Namespace.Application.Events.Error = {}
    );
    
    Namespace.Application.Events.Error.ShowAlert = function() {
    };
    
    Namespace.Application.Events.Error.ShowAlert.prototype.execute = function(){
        alert("H1 has corectly clicked!");
    };
    
    return Namespace.Application.Events.Error.ShowAlert;
});