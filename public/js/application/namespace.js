define([], function(){
    var Namespace = {};
    
    
    Namespace.Application = Namespace.Application || {};
        Namespace.Application.Config || (
            Namespace.Application.Config = {}
        );
        Namespace.Application.Models || (
            Namespace.Application.Models = {}
        );
        Namespace.Application.Collections || (
            Namespace.Application.Collections = {}
        );
        Namespace.Application.Routers || (
            Namespace.Application.Routers = {}
        );
        Namespace.Application.Routes || (
            Namespace.Application.Routes = {}
        );
        Namespace.Application.Events || (
            Namespace.Application.Events = {}
        );
        Namespace.Application.Views || (
            Namespace.Application.Views = {}
        );
        Namespace.Application.Templates || (
            Namespace.Application.Templates = {}
        );
    
    
    Namespace.Core = Namespace.Core || {};
        Namespace.Core.Config || (
            Namespace.Core.Config = {}
        );
        Namespace.Core.Models || (
            Namespace.Core.Models = {}
        );
        Namespace.Core.Collections || (
            Namespace.Core.Collections = {}
        );
        Namespace.Core.Routers || (
            Namespace.Core.Routers = {}
        );
        Namespace.Core.Routes || (
            Namespace.Core.Routes = {}
        );
        Namespace.Core.Events || (
            Namespace.Core.Events = {}
        );
        Namespace.Core.Views || (
            Namespace.Core.Views = {}
        );
        Namespace.Core.Templates || (
            Namespace.Core.Templates = {}
        );
    
    Namespace.Authorization = Namespace.Authorization || {};
        Namespace.Authorization.Config || (
            Namespace.Authorization.Config = {}
        );
        Namespace.Authorization.Models || (
            Namespace.Authorization.Models = {}
        );
        Namespace.Authorization.Collections || (
            Namespace.Authorization.Collections = {}
        );
        Namespace.Authorization.Routers || (
            Namespace.Authorization.Routers = {}
        );
        Namespace.Authorization.Routes || (
            Namespace.Authorization.Routes = {}
        );
        Namespace.Authorization.Events || (
            Namespace.Authorization.Events = {}
        );
        Namespace.Authorization.Views || (
            Namespace.Authorization.Views = {}
        );
        Namespace.Authorization.Templates || (
            Namespace.Authorization.Templates = {}
        );
    
    return Namespace;
});