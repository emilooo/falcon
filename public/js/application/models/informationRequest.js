define([
    "namespace",
    "jquery",
    "underscore",
    "backbone"
], function(Namespace, $, _, Backbone){
    Namespace.Application.Models.InformationRequest = Backbone.Model.extend(
        {
            url: "/rest/information/get",
            initialize: function() {
            },
            defaults: function() {
                return {
                    url: ''
                }
            },
            getRequestInformation: function(urlAddress) {
                this.set({url: urlAddress});
                
                return this.save();
            }
        }
    );
    
    return Namespace.Application.Models.InformationRequest;
});