define([
    'namespace',
    'jquery',
    'underscore',
    'backbone'
], function(Namespace, $, _, Backbone) {
    Namespace.Core.Events.Home || (
        Namespace.Core.Events.Home = {}
    );
    
    Namespace.Core.Events.Home.ShowAlert = function() {
    };
    
    Namespace.Core.Events.Home.ShowAlert.prototype.execute = function(){
        alert("H1 has corectly clicked!");
    };
    
    return Namespace.Core.Events.Home.ShowAlert;
});