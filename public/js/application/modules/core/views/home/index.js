define([
    "namespace",
    "jquery",
    "underscore",
    "backbone",
    "handlebars",
    "modules/core/events/home/showAlert",
    "bootstrap"
], function(Namespace, $, _, Backbone, Handlebars){
    Namespace.Core.Views.Home || (
        Namespace.Core.Views.Home = {}
    );
    
    Namespace.Core.Views.Home.Index = Backbone.View.extend({
        alertEvent: null,
        el: $("body header"),
        initialize: function(){
        },
        events: {
            "click h1": "showAlert"
        },
        render: function() {
            var moduleName = Namespace.Application.Config.Request["module"];
            var controllerName = Namespace.Application.Config.Request["controller"];
            var actionName = Namespace.Application.Config.Request["action"];
            
            console.log("Action: " +actionName + " | Controller: " +controllerName + " | Module: " +moduleName);
        },
        showAlert: function(e){
            this.alertEvent = (this.alertEvent === null)
                ? new Namespace.Core.Events.Home.ShowAlert()
                    : this.alertEvent;
            
            if (this.alertEvent
                instanceof Namespace.Core.Events.Home.ShowAlert) {
                return this.alertEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        }
    });
    
    return Namespace.Core.Views.Home.Index;
});