define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    'modules/core/views/home/index'
], function(Namespace, $, _, Backbone) {
    Namespace.Core.Routes.Home || (
        Namespace.Core.Routes.Home = {}
    );
    
    Namespace.Core.Routes.Home.Index = function(options) {
        this.view
            = (options instanceof Object && options.hasOwnProperty('view'))
            ? options.view : new Namespace.Core.Views.Home.Index();
    };
    
    Namespace.Core.Routes.Home.Index.prototype.makeRoute = function(){
        var viewInstance = this.view;
        
        if (viewInstance instanceof Backbone.View) {
            viewInstance.render();
        } else {
            throw new Error("Incorrect view instance!");
        }
    };
    
    return Namespace.Core.Routes.Home.Index;
});
