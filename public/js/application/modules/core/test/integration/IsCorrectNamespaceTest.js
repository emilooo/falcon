define([
    'namespace'
], function(Namespace){
    test("Namespace for layer of core module", function(){
        assert.ok(Namespace);
        assert.ok(Namespace.Core);
        assert.ok(Namespace.Core.Config);
        assert.ok(Namespace.Core.Models);
        assert.ok(Namespace.Core.Routers);
        assert.ok(Namespace.Core.Routes);
        assert.ok(Namespace.Core.Events);
        assert.ok(Namespace.Core.Collections);
        assert.ok(Namespace.Core.Templates);
        assert.ok(Namespace.Core.Views);
    });
});