define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    'modules/core/routes/home/index'
], function(Namespace, $, _, Backbone) {
    Namespace.Core.Routers.Home || (
        Namespace.Core.Routers.Home = {}
    );
    
    Namespace.Core.Routers.Home = Backbone.Router.extend({
        index: null,
        initialize: function() {
            this.index
                = (this.index instanceof Object)
                    ? this.index
                        : new Namespace.Core.Routes.Home.Index();
        },
        routes: {
          '': 'indexRoute'
        },
        indexRoute: function() {
            var routeInstance = this.login;
            
            if (routeInstance
                instanceof Namespace.Core.Routes.Home.Index) {
                return routeInstance.makeRoute();
            } else {
                throw new Error("Incorrect route instance!");
            }
        }
  });
  
  return Namespace.Core.Routers.Home;
});