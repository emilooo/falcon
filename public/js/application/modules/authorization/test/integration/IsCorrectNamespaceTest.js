define([
    'namespace'
], function(Namespace){
    test("Namespace for layer of authorization module", function(){
        assert.ok(Namespace);
        assert.ok(Namespace.Authorization);
        assert.ok(Namespace.Authorization.Config);
        assert.ok(Namespace.Authorization.Models);
        assert.ok(Namespace.Authorization.Routers);
        assert.ok(Namespace.Authorization.Routes);
        assert.ok(Namespace.Authorization.Events);
        assert.ok(Namespace.Authorization.Collections);
        assert.ok(Namespace.Authorization.Templates);
        assert.ok(Namespace.Authorization.Views);
    });
});