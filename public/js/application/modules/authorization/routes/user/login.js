define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    "/js/application/modules/authorization/views/user/login.js",
], function(Namespace, $, _, Backbone) {
    Namespace.Authorization.Routes.User || (
        Namespace.Authorization.Routes.User = {}
    );
    
    Namespace.Authorization.Routes.User.Login = function(options) {
        this.view
            = (options instanceof Object && options.hasOwnProperty('view'))
            ? options.view : new Namespace.Authorization.Views.User.Login();
    };
    
    Namespace.Authorization.Routes.User.Login.prototype.makeRoute = function(){
        var viewInstance = this.view;
        
        if (viewInstance instanceof Backbone.View) {
            viewInstance.render();
        } else {
            throw new Error("Incorrect view instance!");
        }
    };
    
    return Namespace.Authorization.Routes.User.Login;
});
