define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    'jqueryui',
    'bootstrap'
], function(Namespace, $, _, Backbone) {
    Namespace.Authorization.Events.User || (
        Namespace.Authorization.Events.User = {}
    );
    
    Namespace.Authorization.Events.User.ConfirmLink = function() {
        var id = "confirmBox";
        var subject = "Test message";
        var message = "This is test message";
        var event = null;
        var isPrevented = false;
        var yesButtonValue = "Yes";
        var noButtonValue = "No";
        
        this.setId = function(boxId) {
            id = boxId;
        };
        
        this.getId = function() {
            return id;
        };
        
        this.setSubject = function(dialogSubject) {
            subject = dialogSubject;
        };
        
        this.getSubject = function(){
            return subject;
        };
        
        this.setMessage = function(dialogMessage) {
            message = dialogMessage;
        };
        
        this.getMessage = function() {
            return message;
        };
        
        this.setEvent = function(linkEvent) {
            event = linkEvent;
        };
        
        this.getEvent = function() {
            return event;
        };
        
        this.setYesButtonValue = function(value) {
            yesButtonValue = value;
            
            return this;
        };
        
        this.getYesButtonValue = function() {
            return yesButtonValue;
        }
        
        this.setNoButtonValue = function(value) {
            noButtonValue = value;
            
            return this;
        };
        
        this.getNoButtonValue = function() {
            return noButtonValue;
        }
    };
    
    Namespace.Authorization.Events.User.ConfirmLink.prototype.appendConfirmBoxToBody = function(){
        var dialogTag = document.createElement('div');
        var messageTag = document.createElement('p');
            $(dialogTag).attr('id', this.getId());
            $(dialogTag).attr('title', this.getSubject());
            $(messageTag).append(
                this.getMessage()
            );
            $(dialogTag).append(messageTag);
        $("body").append(dialogTag);
    };
    
    Namespace.Authorization.Events.User.ConfirmLink.prototype.runConfirm = function(){
        var that = this;
        var yesButtonValue = that.getYesButtonValue();
        var noButtonValue = that.getNoButtonValue();
        
        $("#" + that.getId()).dialog({
          resizable: false,
          height:200,
          modal: true,
          close: function(){
              $("#" + that.getId()).remove();
          },
          buttons: {
            [yesButtonValue]: function() {
                $( this ).dialog( "close" );
                $("#" + that.getId()).remove();
                that.isPrevented = true;
                $(that.getEvent().currentTarget).click();
            },
            [noButtonValue]: function() {
                $( this ).dialog( "close" );
                $("#" + that.getId()).remove();
            }
          }
        });
    };
    
    Namespace.Authorization.Events.User.ConfirmLink.prototype.showDialogBox = function(){
        this.getEvent().preventDefault();
        this.appendConfirmBoxToBody();
        this.runConfirm();
        
        return this;
    };
    
    Namespace.Authorization.Events.User.ConfirmLink.prototype.redirectToTarget = function(){
        var linkUrlAddress = $(this.getEvent().currentTarget).attr('href');
        window.location.replace(linkUrlAddress);
        
        return this;
    };
    
    Namespace.Authorization.Events.User.ConfirmLink.prototype.execute = function(){
        if (!this.isPrevented) {
            this.showDialogBox();
        } else {
            this.redirectToTarget();
        }
    };
    
    return Namespace.Authorization.Events.User.ConfirmLink;
});