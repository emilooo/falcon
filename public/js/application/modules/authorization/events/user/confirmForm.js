define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    'jqueryui',
    'bootstrap'
], function(Namespace, $, _, Backbone) {
    Namespace.Authorization.Events.User || (
        Namespace.Authorization.Events.User = {}
    );
    
    Namespace.Authorization.Events.User.ConfirmForm = function() {
        var id = "confirmBox";
        var subject = "Test message";
        var message = "This is test message";
        var formId = "testForm";
        var yesButtonValue = "Yes";
        var noButtonValue = "No";
        
        this.setId = function(boxId) {
            id = boxId;
        };
        
        this.getId = function() {
            return id;
        };
        
        this.setSubject = function(dialogSubject) {
            subject = dialogSubject;
        };
        
        this.getSubject = function(){
            return subject;
        };
        
        this.setMessage = function(dialogMessage) {
            message = dialogMessage;
        };
        
        this.getMessage = function() {
            return message;
        };
        
        this.setFormId = function(formName) {
            formId = formName;
        };
        
        this.getFormId = function() {
            return formId;
        };
        
        this.setYesButtonValue = function(value) {
            yesButtonValue = value;
            
            return this;
        };
        
        this.getYesButtonValue = function() {
            return yesButtonValue;
        }
        
        this.setNoButtonValue = function(value) {
            noButtonValue = value;
            
            return this;
        };
        
        this.getNoButtonValue = function() {
            return noButtonValue;
        }
    };
    
    Namespace.Authorization.Events.User.ConfirmForm.prototype.appendConfirmBoxToBody = function(){
        var dialogTag = document.createElement('div');
        var messageTag = document.createElement('p');
            $(dialogTag).attr('id', this.getId());
            $(dialogTag).attr('title', this.getSubject());
            $(messageTag).append(
                this.getMessage()
            );
            $(dialogTag).append(messageTag);
        $("body").append(dialogTag);
    };
    
    Namespace.Authorization.Events.User.ConfirmForm.prototype.runConfirm = function(){
        var that = this;
        var yesButtonValue = that.getYesButtonValue();
        var noButtonValue = that.getNoButtonValue();
        
        $("#" + that.getId()).dialog({
          resizable: false,
          height:200,
          modal: true,
          close: function(){
              $("#" + that.getId()).remove();
          },
          buttons: {
            [yesButtonValue]: function() {
                $("#" + that.getFormId()).submit();
                $( this ).dialog( "close" );
                $("#" + that.getId()).remove();
            },
            [noButtonValue]: function() {
                $( this ).dialog( "close" );
                $("#" + that.getId()).remove();
            }
          }
        });
    };
    
    Namespace.Authorization.Events.User.ConfirmForm.prototype.isCorrectDialogBox = function(){
        return $("#" + this.getId()).has();
    };
    
    Namespace.Authorization.Events.User.ConfirmForm.prototype.execute = function(){
        this.appendConfirmBoxToBody();
        this.runConfirm();
        return this.isCorrectDialogBox();
    };
    
    return Namespace.Authorization.Events.User.ConfirmForm;
});