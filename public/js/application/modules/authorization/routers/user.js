define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    '/js/application/modules/authorization/routes/user/login.js',
    '/js/application/modules/authorization/routes/user/register.js',
    '/js/application/modules/authorization/routes/user/index.js',
    '/js/application/modules/authorization/routes/user/create.js',
    '/js/application/modules/authorization/routes/user/profile.js'
], function(Namespace, $, _, Backbone) {
    Namespace.Authorization.Routers.User || (
        Namespace.Authorization.Routers.User = {}
    );
    
    Namespace.Authorization.Routers.User = Backbone.Router.extend({
        login: null,
        register: null,
        index: null,
        create: null,
        profile: null,
        initialize: function() {
            this.login
                = (this.login instanceof Object)
                    ? this.login
                        : new Namespace.Authorization.Routes.User.Login();
            this.register
                = (this.register instanceof Object)
                    ? this.register
                        : new Namespace.Authorization.Routes.User.Register();
            this.index
                = (this.index instanceof Object)
                    ? this.index
                        : new Namespace.Authorization.Routes.User.Index();
            this.create
                = (this.create instanceof Object)
                    ? this.create
                        : new Namespace.Authorization.Routes.User.Create();
                        
            this.profile
                = (this.profile instanceof Object)
                    ? this.profile
                        : new Namespace.Authorization.Routes.User.Profile();
        },
        routes: {
            '': 'selectRoute'
        },
        selectRoute: function(){
            var actionName = Namespace.Application.Config.Request["action"];
            var routeName = actionName + "Route";
            
            switch (routeName) {
                case 'loginRoute': this.loginRoute();break;
                case 'registerRoute': this.registerRoute(); break;
                case 'indexRoute': this.indexRoute(); break;
                case 'createRoute': this.createRoute();break;
                case 'profileRoute': this.profileRoute();break;
                default: throw new Error("Route " + routeName + " not exists!");
            }
        },
        loginRoute: function(){
            var routeInstance = this.login;
            
            if (routeInstance
                instanceof Namespace.Authorization.Routes.User.Login) {
                return routeInstance.makeRoute();
            } else {
                throw new Error("Incorrect route instance!");
            }
        },
        registerRoute: function(){
            var routeInstance = this.register;
            
            if (routeInstance
                instanceof Namespace.Authorization.Routes.User.Register) {
                return routeInstance.makeRoute();
            } else {
                throw new Error("Incorrect route instance!");
            }
        },
        indexRoute: function(){
            var routeInstance = this.index;
            
            if (routeInstance
                instanceof Namespace.Authorization.Routes.User.Index) {
                return routeInstance.makeRoute();
            } else {
                throw new Error("Incorrect route instance!");
            }
        },
        createRoute: function(){
            var routeInstance = this.create;
            
            if (routeInstance
                instanceof Namespace.Authorization.Routes.User.Create) {
                return routeInstance.makeRoute();
            } else {
                throw new Error("Incorrect route instance!");
            }
        },
        profileRoute: function(){
            var routeInstance = this.profile;
            
            if (routeInstance
                instanceof Namespace.Authorization.Routes.User.Profile) {
                return routeInstance.makeRoute();
            } else {
                throw new Error("Incorrect route instance!");
            }
        }
  });
  
  return Namespace.Authorization.Routers.User;
});