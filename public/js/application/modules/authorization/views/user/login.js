define([
    "namespace",
    "jquery",
    "underscore",
    "backbone",
    "handlebars",
    "i18next",
    "/js/application/modules/authorization/events/user/confirmForm.js",
    "bootstrap"
], function(Namespace, $, _, Backbone, Handlebars, i18next){
    Namespace.Authorization.Views.User || (
        Namespace.Authorization.Views.User = {}
    );
    
    Namespace.Authorization.Views.User.Login
        = Backbone.View.extend({
        confirmFormEvent: null,
        el: $("body #main main"),
        initialize: function(){
        },
        events: {
            "click input[name='login']": "loginUser"
        },
        render: function() {
            $("#userForm input[type='submit']").attr('type', 'button');
        },
        loginUser: function(event){
            this.confirmFormEvent = (this.confirmFormEvent === null)
                ? new Namespace.Authorization.Events.User.ConfirmForm()
                    : this.confirmFormEvent;
            
            if (this.confirmFormEvent
                instanceof Namespace.Authorization.Events.User.ConfirmForm) {
                this.confirmFormEvent.setSubject(
                    i18next.t("Please confirm your decision")
                );
                this.confirmFormEvent.setMessage(
                    i18next.t("Do you want to login?")
                );
                this.confirmFormEvent.setYesButtonValue(i18next.t("Yes"));
                this.confirmFormEvent.setNoButtonValue(i18next.t("No"));
                this.confirmFormEvent.setFormId("userForm");
                
                return this.confirmFormEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        }
    });
    
    return Namespace.Authorization.Views.User.Login;
});