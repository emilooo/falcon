define([
    "namespace",
    "jquery",
    "underscore",
    "backbone",
    "handlebars",
    "i18next",
    "/js/application/modules/authorization/events/user/confirmLink.js",
    "/js/application/modules/authorization/events/user/confirmForm.js",
    "bootstrap"
], function(Namespace, $, _, Backbone, Handlebars, i18next){
    Namespace.Authorization.Views.User || (
        Namespace.Authorization.Views.User = {}
    );
    
    Namespace.Authorization.Views.User.Create
        = Backbone.View.extend({
        confirmLinkEvent: null,
        confirmFormEvent: null,
        el: $("body div.container"),
        initialize: function(){
        },
        events: {
            "click a[class='showProfile']": "showProfile",
            "click a[class='logoutUser']": "logoutUser",
            "click a[id='saveIcon']": "saveUser",
            "click a[id='refreshResults']": "refreshPage"
        },
        render: function() {
        },
        showProfile: function(event){
        },
        logoutUser: function(event){
            this.confirmLinkEvent = (this.confirmLinkEvent === null)
                ? new Namespace.Authorization.Events.User.ConfirmLink()
                    : this.confirmLinkEvent;
            
            if (this.confirmLinkEvent
                instanceof Namespace.Authorization.Events.User.ConfirmLink) {
                this.confirmLinkEvent.setSubject(
                    i18next.t("Please confirm your decision")
                );
                this.confirmLinkEvent.setMessage(
                    i18next.t("Do you want to logout current user?")
                );
                this.confirmLinkEvent.setYesButtonValue(i18next.t("Yes"));
                this.confirmLinkEvent.setNoButtonValue(i18next.t("No"));
                this.confirmLinkEvent.setEvent(event);
                
                return this.confirmLinkEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        },
        saveUser: function(event){
            this.confirmFormEvent = (this.confirmFormEvent === null)
                ? new Namespace.Authorization.Events.User.ConfirmForm()
                    : this.confirmFormEvent;
            
            if (this.confirmFormEvent
                instanceof Namespace.Authorization.Events.User.ConfirmForm) {
                this.confirmFormEvent.setSubject(
                    i18next.t("Please confirm your decision")
                );
                this.confirmFormEvent.setMessage(
                    i18next.t("Do you want to create the new user?")
                );
                this.confirmFormEvent.setYesButtonValue(i18next.t("Yes"));
                this.confirmFormEvent.setNoButtonValue(i18next.t("No"));
                this.confirmFormEvent.setFormId("userForm");
                
                return this.confirmFormEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        },
        refreshPage: function(event){
        }
    });
    
    return Namespace.Authorization.Views.User.Create;
});