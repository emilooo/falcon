define([
    "namespace",
    "jquery",
    "underscore",
    "backbone",
    "handlebars",
    "i18next",
    "/js/application/modules/authorization/events/user/confirmLink.js",
    "/js/application/modules/authorization/events/user/confirmForm.js",
    "bootstrap"
], function(Namespace, $, _, Backbone, Handlebars, i18next){
    Namespace.Authorization.Views.User || (
        Namespace.Authorization.Views.User = {}
    );
    
    Namespace.Authorization.Views.User.Index
        = Backbone.View.extend({
        confirmLinkEvent: null,
        confirmFormEvent: null,
        el: $("body div.container"),
        initialize: function(){
        },
        events: {
            "click a[class='showProfile']": "showProfile",
            "click a[class='logoutUser']": "logoutUser",
            "click a[id='createRecord']": "createUser",
            "click a[id='deleteRecord']": "deleteUser",
            "click a[id='refreshResults']": "refreshPage",
            "click a[id='checkAllResults']": "selectItems",
            "click a[id='removeAllCheckedResults']": "deleteRecords",
            "change input[type=checkbox]": "selectItem"
        },
        render: function() {
        },
        selectItem: function(event){
            var checkbox = $(event.currentTarget);
            
            if ($(checkbox).is(":checked")) {
                $(checkbox).parent().parent().parent().parent().addClass("active");
            } else {
                $(checkbox).parent().parent().parent().parent().removeClass("active");
            }
        },
        selectItems: function(event){
            var checkboxes = $('input[type="checkbox"]');
            $.each(checkboxes, function(key, value){
                if ($(value).is(":checked")) {
                    $(value).prop("checked", false);
                    $(value).parent().parent().parent().parent().removeClass("active");
                } else {
                    $(value).prop("checked", true);
                    $(value).parent().parent().parent().parent().addClass("active");
                }
            });
            event.preventDefault();
        },
        deleteRecords: function(event){
            alert(i18next.t("This option is unsupported!"));
            event.preventDefault();
        },
        showRecord: function(event){
        },
        showProfile: function(event){
        },
        logoutUser: function(event){
            this.confirmLinkEvent = (this.confirmLinkEvent === null)
                ? new Namespace.Authorization.Events.User.ConfirmLink()
                    : this.confirmLinkEvent;
            
            if (this.confirmLinkEvent
                instanceof Namespace.Authorization.Events.User.ConfirmLink) {
                this.confirmLinkEvent.setSubject(
                    i18next.t("Please confirm your decision")
                );
                this.confirmLinkEvent.setMessage(
                    i18next.t("Do you want to logout current user?")
                );
                this.confirmLinkEvent.setYesButtonValue(i18next.t("Yes"));
                this.confirmLinkEvent.setNoButtonValue(i18next.t("No"));
                this.confirmLinkEvent.setEvent(event);
                
                return this.confirmLinkEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        },
        createUser: function(event){
            this.confirmLinkEvent = (this.confirmLinkEvent === null)
                ? new Namespace.Authorization.Events.User.ConfirmLink()
                    : this.confirmLinkEvent;
            
            if (this.confirmLinkEvent
                instanceof Namespace.Authorization.Events.User.ConfirmLink) {
                this.confirmLinkEvent.setSubject(
                    i18next.t("Please confirm your decision")
                );
                this.confirmLinkEvent.setMessage(
                    i18next.t("Do you want to create the new user?")
                );
                this.confirmLinkEvent.setYesButtonValue(i18next.t("Yes"));
                this.confirmLinkEvent.setNoButtonValue(i18next.t("No"));
                this.confirmLinkEvent.setEvent(event);
                
                return this.confirmLinkEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        },
        deleteUser: function(event){
            this.confirmLinkEvent = (this.confirmLinkEvent === null)
                ? new Namespace.Authorization.Events.User.ConfirmLink()
                    : this.confirmLinkEvent;
                
            if (this.confirmLinkEvent
                instanceof Namespace.Authorization.Events.User.ConfirmLink) {
                this.confirmLinkEvent.setSubject(
                    i18next.t("Please confirm your decision")
                );
                this.confirmLinkEvent.setMessage(
                    i18next.t("Do you want to delete this user?")
                );
                this.confirmLinkEvent.setYesButtonValue(i18next.t("Yes"));
                this.confirmLinkEvent.setNoButtonValue(i18next.t("No"));
                this.confirmLinkEvent.setEvent(event);
                
                return this.confirmLinkEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        },
        refreshPage: function(event){
        }
    });
    
    return Namespace.Authorization.Views.User.Index;
});
