define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    '/js/application/routes/error/error.js'
], function(Namespace, $, _, Backbone){
    Namespace.Application.Routers.Error = Backbone.Router.extend({
        error: null,
        initialize: function(){
            this.error
                = (this.error instanceof Object)
                    ? this.error
                        : new Namespace.Application.Routes.Error.Error();
        },
        routes: {
          '': 'errorRoute'
        },
        errorRoute: function(){
            var routeInstance = this.login;
            
            if (routeInstance
                instanceof Namespace.Application.Routes.Error.Error) {
                return routeInstance.makeRoute();
            } else {
                throw new Error("Incorrect route instance!");
            }
        }
    });
    
    return Namespace.Application.Routers.Error;
});