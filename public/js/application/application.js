define([
   'namespace',
  'jquery',
  'initRouters',
  "i18next"
], function(namespace, $, InitRouters, i18next){
    i18next.init({
        lng: $("html").attr('lang'),
        resources: {
            "pl": {
                "translation": {
                    "Please confirm your decision": "Proszę potwierdź swoją decyzję",
                    "Do you want to logout current user?": "Czy chcesz wylogować bierzącego użytkownika?",
                    "Do you want to login?": "Czy chcesz się zalogować?",
                    "Do you want to create the new user?": "Czy chcesz utworzyć nowego użytkownika?",
                    "Do you want to register the new user?": "Czy chcesz zarejestrować nowego użytkownika?",
                    "Do you want to delete this user?": "Czy chcesz usunąć użytkownika?",
                    "This option is unsupported!": "Ta opcja jest niedostępna!",
                    "Yes": "Tak",
                    "No": "Nie"
                }
            }
        }
    });
    
    var initialize = function(){
        InitRouters.initialize();
    }
    
    return {
      initialize: initialize
    };
});