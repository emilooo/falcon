require.config({
    baseUrl: "/js/application/",
    paths: {
        namespace: '/js/application/namespace',
        jquery: '/js/library/jquery/jquery',
        jqueryui: "/js/library/jquery-ui/jquery-ui",
        underscore: "/js/library/underscore/underscore",
        backbone: "/js/library/backbone/backbone",
        handlebars: '/js/library/handlebars/handlebars',
        hbs: '/js/library/hbs/hbs',
        bootstrap: "/js/library/bootstrap-sass/bootstrap",
        i18next: "/js/library/i18next/i18next.min"
    },
    shim: {
        namespace: {
            exports: "Namespace"
        },
        jquery: {
            exports: "$"
        },
        jqueryui: {
            exports: "$",
            deps: ["jquery"]
        },
        underscore: {
            exports: "_"
        },
        backbone: {
            deps: ["namespace","jquery","underscore"],
            exports: "Backbone"
        }
    },
    hbs: {
        disableI18n: true
    },
    i18next: {
        lng: "en",
        fallbackLng: "en",
        detectLngQS: "locale",
        lowerCaseLng: true,
        useCookie: false,
        resGetPath: "__ns__.__lng__.json",
        debug: true,
        saveMissingTo: "fallback",
        load: "all"
    }
});

require([
    'jquery',
    'application',
], function($, Application){
    Application.initialize();
});