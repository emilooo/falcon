define([
    "namespace",
    "jquery",
    "underscore",
    "backbone",
    "handlebars",
    "/js/application/events/error/showAlert.js",
    "bootstrap"
], function(Namespace, $, _, Backbone, Handlebars){
    Namespace.Application.Views.Error || (
        Namespace.Application.Views.Error = {}
    );
    
    Namespace.Application.Views.Error.Error = Backbone.View.extend({
        alertEvent: null,
        el: $("body section.page-content"),
        events: {
            "click h1": "showAlert"
        },
        render: function() {
            var moduleName = Namespace.Application.Config.Request["module"];
            var controllerName = Namespace.Application.Config.Request["controller"];
            var actionName = Namespace.Application.Config.Request["action"];
            
            console.log("Action: " +actionName + " | Controller: " +controllerName + " | Module: " +moduleName);
        },
        showAlertEvent: function(){
            this.alertEvent = (this.alertEvent === null)
                ? new Namespace.Application.Events.Error.ShowAlert()
                    : this.alertEvent;
            
            if (this.alertEvent
                instanceof Namespace.Application.Events.Error.ShowAlert) {
                return this.alertEvent.execute();
            } else {
                throw new Error("Incorrect event instance!");
            }
        }
    });
    
    return Namespace.Application.Views.Error.Error;
});