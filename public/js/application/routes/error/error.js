define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    '/js/application/views/error/error.js'
], function(Namespace, $, _, Backbone) {
    Namespace.Application.Routes.Error || (
        Namespace.Application.Routes.Error = {}
    );
    
    Namespace.Application.Routes.Error.Error = function(options) {
        this.view
            = (options instanceof Object && options.hasOwnProperty('view'))
            ? options.view : new Namespace.Application.Views.Error.Error();
    };
    
    Namespace.Application.Routes.Error.Error.prototype.makeRoute = function(){
        var viewInstance = this.view;
        
        if (viewInstance instanceof Backbone.View) {
            viewInstance.render();
        } else {
            throw new Error("Incorrect view instance!");
        }
    };
    
    return Namespace.Application.Routes.Error.Error;
});