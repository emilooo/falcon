define([
    'namespace',
    'jquery',
    'underscore',
    'backbone',
    'models/informationRequest',
    'routers/error/error'
], function(
    Namespace,
    $,
    _,
    Backbone,
    InformationRequestModel,
    ErrorRouter
){
    var initializeRouter = function(){
        var informationRequestModel = new InformationRequestModel();
        var getRequestInformationResult
                = informationRequestModel
                    .getRequestInformation(window.location.href);
        $.when(getRequestInformationResult).then(function(){
            Namespace.Application.Config.Request
                = getRequestInformationResult.responseJSON;
            var actionName = Namespace.Application.Config.Request["action"];
            var controllerName
                = Namespace.Application.Config.Request['controller'];
            var moduleName
                = Namespace.Application.Config.Request["module"];
            
            try {
                var routerObject = require(["modules/" + moduleName + "/routers/" + controllerName], function(routerClass){
                    if (typeof routerClass === "function") {
                        var routerInstance = new routerClass();
                    } else {
                        Namespace.Application.Config.Request["module"] = null;
                        Namespace.Application.Config.Request["controller"] = "error";
                        Namespace.Application.Config.Request["action"] = "error";
                        
                        var routerInstance = new ErrorRouter();
                    }
                    
                    Backbone.emulateHTTP = true;
                    Backbone.emulateJSON = false; 
                    Backbone.history.start();
                });
            } catch(error) {
                throw error;
            }
        });
        
        return this;
    }
    
    return {
        initialize: initializeRouter,
    }
});