require.config({
    baseUrl: '/js/test/',
    urlArgs: 'bust=' + (new Date()).getTime(),
    paths: {
        'namespace': '/js/application/namespace',
        'jquery': '/js/library/jquery/jquery',
        'jqueryui': "/js/library/jquery-ui/jquery-ui",
        'underscore': "/js/library/underscore/underscore",
        'backbone': "/js/library/backbone/backbone",
        'handlebars': '/js/library/handlebars/handlebars',
        'hbs': '/js/library/hbs/hbs',
        'bootstrap': "/js/library/bootstrap-sass/bootstrap",
        'mocha' : '/js/library/mocha/mocha',
        'chai' : '/js/library/chai/chai',
        'sinon': '/js/library/sinonjs/sinon'
    },
    shim: {
        namespace: {
            exports: "Namespace"
        }
    }
});

define([
    'chai',
    'mocha',
    'sinon'
],function(chai) {
    assert = chai.assert;
    mocha.setup('tdd');
    require([
        '/js/test/application/integration/IsCorrectNamespaceTest.js',
        '/js/test/application/integration/models/InformationRequest/IsCorrectModelTest.js',
        '/js/test/application/integration/routers/Error/IsCorrectRouterTest.js',
        '/js/test/application/integration/routes/Error/Error/IsCorrectRouteTest.js',
        '/js/test/application/integration/views/Error/Error/IsCorrectViewTest.js',
        '/js/application/modules/core/test/integration/IsCorrectNamespaceTest.js',
        '/js/application/modules/authorization/test/integration/IsCorrectNamespaceTest.js'
    ], function() {
        mocha.run();
    });
}); 