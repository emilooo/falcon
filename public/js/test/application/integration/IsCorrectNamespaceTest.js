define([
    'namespace'
], function(Namespace){
    test("Namespace for layer of application", function(){
        assert.ok(Namespace);
        assert.ok(Namespace.Application);
        assert.ok(Namespace.Application.Config);
        assert.ok(Namespace.Application.Models);
        assert.ok(Namespace.Application.Routers);
        assert.ok(Namespace.Application.Routes);
        assert.ok(Namespace.Application.Events);
        assert.ok(Namespace.Application.Collections);
        assert.ok(Namespace.Application.Templates);
        assert.ok(Namespace.Application.Views);
    });
});