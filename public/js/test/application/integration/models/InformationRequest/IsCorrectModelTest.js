define([
    'namespace',
    '/js/application/models/informationRequest.js'
], function(Namespace){
    test("Namespace.Application.Models.InformationRequest.IsCorrectModelTest", function(){
        var informationModel
            = new Namespace.Application.Models.InformationRequest();
        
        assert.instanceOf(informationModel, Backbone.Model);
        assert.equal(informationModel.url, '/rest/information/get');
        assert.property(informationModel, 'getRequestInformation');
    });
});
