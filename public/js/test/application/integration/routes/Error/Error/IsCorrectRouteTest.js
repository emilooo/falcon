define([
    'namespace',
    '/js/application/routes/error/error.js'
], function(Namespace){
    test("Namespace.Application.Routes.Error.Error.IsCorrectRouterTest", function(){
        var errorRoute
            = new Namespace.Application.Routes.Error.Error();
        
        assert.property(errorRoute, "makeRoute");
    });
});