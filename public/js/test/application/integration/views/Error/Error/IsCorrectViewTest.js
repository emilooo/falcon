define([
    'namespace',
    '/js/application/views/error/error.js'
], function(Namespace){
    test("Namespace.Application.Views.Error.Error", function(){
        var errorView = new Namespace.Application.Views.Error.Error();
        
        assert.instanceOf(errorView, Backbone.View);
        assert.property(errorView, 'showAlertEvent');
    });
});
