define([
    'namespace',
    '/js/application/routers/error/error.js'
], function(Namespace){
    test("Namespace.Application.Routers.Error.IsCorrectRouterTest", function(){
        var errorRouter
            = new Namespace.Application.Routers.Error();
        
        assert.instanceOf(errorRouter, Backbone.Router);
        assert.property(errorRouter, "errorRoute");
    });
});